package org.gzoumix.lib;

import org.gzoumix.lib.agent.Runtime;
import org.gzoumix.lib.agent.runtime.RuntimeSignal;
import org.gzoumix.lib.agent.runtime.RuntimeTaskColor;
import org.gzoumix.lib.agent.runtime.RuntimeThread;
import org.gzoumix.lib.agent.task.Task;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestRuntime {

  private static final int NB_THREADS  = 2;
  private static final int NB_MESSAGES = 5;


  public static class Actor extends Task {

    private RuntimeSignal ping;
    private RuntimeSignal pong;
    private String message;

    public final List<RuntimeSignal> waiting;

    public Actor(RuntimeSignal ping, RuntimeSignal pong, String message) {
      this.ping = ping;
      this.pong = pong;
      this.message = message;
      this.waiting = new ArrayList<>(2);
      this.waiting.add(pong);
    }

    @Override
    public void run(RuntimeThread thread) {
      System.out.println("[" + this + "]: " + this.message);
      System.out.println("[" + this + "]: " + "triggering " + this.ping);
      this.ping.trigger();
      this.publish(this, this.waiting);
      System.out.println("[" + this + "]: " + "new cycle");
    }

  }




  @Test
  public void testPingPong() throws Exception {
    Runtime runtime = new Runtime();
    System.out.println("Runtime = " + runtime);
    RuntimeThread t1 = new RuntimeThread(runtime);
    RuntimeThread t2 = new RuntimeThread(runtime);
    System.out.println("The two threads: (tping = " + t1 + ", tpong = " + t2 + ")");

    runtime.add(t1);
    runtime.add(t2);

    RuntimeSignal sping = new RuntimeSignal(runtime);
    RuntimeSignal spong = new RuntimeSignal(runtime);
    System.out.println("The two signals: (sping = " + sping + ", spong = " + spong + ")");

    Actor ping = new Actor(sping, spong, "  PING ???");
    Actor pong = new Actor(spong, sping, "  PONG !!!");
    System.out.println("The two actors: (ping = " + ping + ", pong = " + pong + ")");
    System.out.println(ping + " waits for " + spong);
    System.out.println(pong + " waits for " + sping);


    RuntimeTaskColor cping = t1.newColor(ping, ping.waiting);
    RuntimeTaskColor cpong = t2.newColor(pong, pong.waiting);
    System.out.println("The two colors: (cping = " + cping + ", cpong = " + cpong + ")");



    System.out.println("Starting");
    runtime.start();
    System.out.println("triggering " + sping);
    sping.trigger();

    Thread.sleep(5000);
    System.out.println("!! stop !!");
    runtime.stop();
  }
}
