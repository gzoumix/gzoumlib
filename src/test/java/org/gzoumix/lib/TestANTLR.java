/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib;

import org.gzoumix.lib.syntax.*;
import org.gzoumix.lib.syntax.stgenerator.STGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.stringtemplate.v4.AutoIndentWriter;
import org.stringtemplate.v4.ST;

import java.io.*;

public class TestANTLR {

//  static final String grammar = "src/test/antlr/Expression.g4";
//  static final String st = "src-gen/testSTGenerator/resources/Expression.stg";
//  static final String java = "src-gen/testSTGenerator/java/org/gzoumix/lib/syntax/PrintExpression.java";

  static final String grammar = "src/test/antlr/ASM6502.g4";
  static final String st = "src-gen/test-st/resources/ASM6502.stg";
  static final String java = "src-gen/test-st/java/org/gzoumix/lib/syntax/PrintASM6502.java";


  public static void main(String[] args) throws IOException {
    STGenerator generator = new STGenerator(grammar);
    generator.setFileStringTemplate(st);

    generator.setFileCode(java);

    generator.setPackageName("org.gzoumix.lib.syntax");
    generator.generate();
  }

}
