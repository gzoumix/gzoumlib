package org.gzoumix.lib;

import org.gzoumix.lib.data.ArrayQueue;
import org.gzoumix.lib.data.ArrayQueueExtensible;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestArrayQueue {
  private Queue<Integer> queue;
  private int capacity = 16;

  @Before/*this method will be run before each '@Test' due to the '@Before' annotation. //use it to initialize data structures, and other things you need to do before each test*/ public void init() {
    this.queue = new ArrayQueue<>(this.capacity);
  }

  @Test
  public void testIsEmptyOnEmpty() {
    assertTrue(this.queue.isEmpty());
    assertTrue(this.queue.size() == 0);
  }

  @Test
  public void testIsEmptyOnNotEmpty() {
    this.queue.offer(5);
    assertTrue(!this.queue.isEmpty());
  }

  @Test
  public void testOneEnqueue() {
    this.queue.offer(5);
  }

  @Test
  public void testOneDequeue() {
    Integer tmp = new Integer(5);
    this.queue.offer(tmp);
    assertTrue(tmp == this.queue.poll());
  }

  @Test
  public void testOrdering() {
    this.queue.offer(1);
    this.queue.offer(2);
    this.queue.offer(3);
    assertEquals(1, queue.poll().intValue());
    assertEquals(2, queue.poll().intValue());
    assertEquals(3, queue.poll().intValue());
  }

  @Test(expected = NoSuchElementException.class)
  public void testDequeueOnEmpty() {
    this.queue.remove();
  }

  @Test
  public void testEmptyAfterDequeue() {
    this.queue.offer(5);
    this.queue.poll();
    assertTrue(this.queue.isEmpty());
  }

  private void testNOperations(int n) {
    Random r = new Random();
    LinkedList<Integer> list = new LinkedList<>();
    int num;
    for (int i = 0; i < n; i++) {
      if (this.queue.isEmpty() || (r.nextInt(3) < 2)) {
        num = new Integer(r.nextInt(100));
        if (!this.queue.offer(num)) {
          assertTrue("The queue contains less elements than its capacity but raised an exception nonetheless", this.queue.size() >= this.capacity);
          break;
        }
        list.addFirst(num);
      }
    }/*now that we're done going through n operations, element until empty and compare results*/
    while (!this.queue.isEmpty()) {
      assertTrue("list is empty but queue isn't", !list.isEmpty());
      assertTrue("End elements do not match (capacity=" + this.capacity + ", size=" + this.queue.size() + ")", list.removeLast() == queue.poll());
    }
    assertTrue("queue is empty but list isn't", list.isEmpty());
  }

  @Test
  public void test10() {
    testNOperations(10);
  }

  @Test
  public void test100() {
    testNOperations(100);
  }

  @Test
  public void test10000() {
    testNOperations(10000);
  }

  @Test
  public void testMillion() {
    testNOperations(1000000);
  }

  @Test
  public void testArrayQueueExtensible() {
    this.queue = new ArrayQueueExtensible(this.capacity);
    testNOperations(1000);
  }
}