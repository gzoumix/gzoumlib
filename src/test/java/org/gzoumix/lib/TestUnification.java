/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib;

import org.gzoumix.lib.solver.unification.ITerm;
import org.gzoumix.lib.solver.unification.Constraint;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestUnification {

  // what to test? I think there is something missing: when there are two images for the same variable.

  public static class TStructured implements ITerm<String> {

    private String constructor;
    private List<ITerm> subterms;

    public TStructured(String constructor, List<ITerm> subterms) {
      this.constructor = constructor;
      this.subterms = subterms;
    }

    public TStructured(String constructor) {
      this(constructor, new ArrayList<ITerm>());
    }

    public TStructured(String constructor, ITerm sub) {
      this(constructor);
      this.subterms.add(sub);
    }

    public TStructured(String constructor, ITerm sub1, ITerm sub2) {
      this(constructor);
      this.subterms.add(sub1);
      this.subterms.add(sub2);
    }

    @Override
    public boolean isVariable() { return false; }

    @Override
    public String getConstructor() { return this.constructor; }

    @Override
    public List<ITerm> getSubterms() { return this.subterms; }

    @Override
    public String toString() { return constructor + subterms; }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      TStructured that = (TStructured) o;

      if (constructor != null ? !constructor.equals(that.constructor) : that.constructor != null) return false;
      return !(subterms != null ? !subterms.equals(that.subterms) : that.subterms != null);

    }

    @Override
    public int hashCode() {
      int result = constructor != null ? constructor.hashCode() : 0;
      result = 31 * result + (subterms != null ? subterms.hashCode() : 0);
      return result;
    }
  }

  public static class TVariable implements ITerm<Object> {

    private String name;

    public TVariable(String name) { this.name = name; }

    @Override
    public boolean isVariable() { return true; }

    @Override
    public Object getConstructor() { return null; }

    @Override
    public List<ITerm> getSubterms() { return null; }

    public String getName() { return name; }

    @Override
    public String toString() { return name; }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      TVariable tVariable = (TVariable) o;

      return !(name != null ? !name.equals(tVariable.name) : tVariable.name != null);
    }

    @Override
    public int hashCode() { return name != null ? name.hashCode() : 0; }
  }


  /*
    alpha = S(beta)
    S(beta) = S(D(alpha, gamma))
    gamma = NULL

    =>
     alpha => S(beta)
     S(beta) = S(D(alpha, gamma))
     beta => D(alpha, gamma)
     gamma => NULL
         ======> OK
   */

  @Test
  public void testUnification() throws Exception {
    TVariable alpha = new TVariable("alpha");
    TVariable beta = new TVariable("beta");
    TVariable gamma = new TVariable("gamma");

    ITerm eq1left = alpha;
    ITerm eq1right = new TStructured("S", beta);

    ITerm eq2left = new TStructured("S", beta);
    ITerm eq2right = new TStructured("S", new TStructured("D", alpha, gamma));

    ITerm eq3left = gamma;
    ITerm eq3right = new TStructured("NULL");

    System.out.println("Adding the edges to the constraint");

    Constraint<String> constraint = new Constraint<>();
    constraint.add("EQ1", eq1left, eq1right);
    constraint.add("EQ2", eq2left, eq2right);
    constraint.add("EQ3", eq3left, eq3right);

    System.out.println("Solving the constraint");

    constraint.solve();

    System.out.println("Solved Constraint:\n" + constraint.getGraph());
  }
}
