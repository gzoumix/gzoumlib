/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.environment;

import org.gzoumix.lib.data.Log;
import org.junit.Test;

import java.util.*;

public class TestEnvironment {


  /** 1. definitions */
  private static class Definition implements IDeclaration<String, Definition> {
    private String _name;
    private List<EnvironmentDeclatation.PathElement<String>> _solved;

    public Definition(String name) { _name = name; _solved = new LinkedList<>(); }
    public void reset() { _solved = new LinkedList<>(); }

    @Override
    public void resolve(EnvironmentDeclatation.PathElement<String> path, Definition def) { _solved.add(path); }
    @Override
    public String getName() { return _name; }
    @Override
    public String toString() { return getName() + " : " + _solved.toString(); }
  }

  private Definition _def1;
  private Definition _def2;
  private Definition _def3;
  private Definition _def4;

  private void initDefinitions() {
    _def1 = new Definition("def1");
    _def2 = new Definition("def2");
    _def3 = new Definition("def3");
    _def4 = new Definition("def4");
  }


  private void resetDefinitions() {
    _def1.reset(); _def2.reset(); _def3.reset(); _def4.reset();
  }

  /** 2. paths */
  private EnvironmentDeclatation.PathElement<String> _pathElement1;
  private EnvironmentDeclatation.PathElement<String> _pathElement2;
  private EnvironmentDeclatation.PathElement<String> _pathElement3;
  private EnvironmentDeclatation.PathElement<String> _pathElement3bis;
  private EnvironmentDeclatation.PathElement<String> _pathElement4;

  private EnvironmentDeclatation.PathStar<String> _pathStar1;
  private EnvironmentDeclatation.PathStar<String> _pathStar2;
  private EnvironmentDeclatation.PathStar<String> _pathStar3;


  private void initPaths() {
    _pathElement1 = new EnvironmentDeclatation.PathElement<>();
    _pathElement1.add("module1"); _pathElement1.add("submodule1"); _pathElement1.add("El1");

    _pathElement2 = new EnvironmentDeclatation.PathElement<>();
    _pathElement2.add("module1"); _pathElement2.add("submodule1"); _pathElement2.add("El2");

    _pathElement3 = new EnvironmentDeclatation.PathElement<>();
    _pathElement3.add("module1"); _pathElement3.add("submodule2"); _pathElement3.add("El3");

    _pathElement3bis = new EnvironmentDeclatation.PathElement<>();
    _pathElement3bis.add("toto");

    _pathElement4 = new EnvironmentDeclatation.PathElement<>();
    _pathElement4.add("module2"); _pathElement4.add("El4");


    _pathStar1 = new EnvironmentDeclatation.PathStar<>();
    _pathStar1.add("module1"); _pathStar1.add("submodule1");

    _pathStar2 = new EnvironmentDeclatation.PathStar<>();
    _pathStar2.add("module1"); _pathStar2.add("submodule2");

    _pathStar3 = new EnvironmentDeclatation.PathStar<>();
    _pathStar3.add("module2"); _pathStar3.add("submodule1");
  }


  /** 3. Tests */

  private Log _log = new Log();

  @Test
  public void test1() throws Exception {
    initDefinitions();
    initPaths();

    EnvironmentDeclatation<String, Definition> env = new EnvironmentDeclatation<>();
    env.addSearchPath(EnvironmentDeclatation.emptyOpenPath);

    _log.logInfo("Test Environment: test 1 starting...");
    _log.indent();
    _log.logInfo("initial state of the environment:");
    _log.indent();_log.logInfo(env.toString());_log.dedent();
    _log.toStream(System.out); _log.clear();

    _log.logInfo("Adding the first definition");
    env.put(_pathElement1, _def1, null);
    _log.logInfo("Adding the second definition");
    env.put(_pathElement2, _def2, null);
    _log.logInfo("Adding the third definition");
    env.put(_pathElement3, _def3, null);
    _log.toStream(System.out); _log.clear();

    _log.logInfo("Final state of the environment:");
    _log.indent();_log.logInfo(env.toString());_log.dedent();
    _log.dedent();
    _log.logInfo("Test Environment: test 1 finished");
    _log.toStream(System.out); _log.clear();
  }

  @Test
  public void test2() throws Exception {
    initDefinitions();
    initPaths();

    EnvironmentDeclatation<String, Definition> env = new EnvironmentDeclatation<>();
    env.addSearchPath(EnvironmentDeclatation.emptyOpenPath);

    _log.logInfo("Test Environment: test 2 starting...");
    _log.indent();
    _log.logInfo("initial state of the environment:");
    _log.indent();_log.logInfo(env.toString());_log.dedent();
    _log.toStream(System.out); _log.clear();

    Set<EnvironmentDeclatation.PathElement<String>> set = new HashSet<>();

    _log.logInfo("Adding the first definition");
    env.put(_pathElement1, _def1, null);
    _log.logInfo("Adding the second definition");
    set.add(_pathElement1);
    env.put(_pathElement2, _def2, new HashSet<EnvironmentDeclatation.PathElement<String>>(set));
    _log.logInfo("Adding the third definition");
    set.add(_pathElement2);
    env.put(_pathElement3, _def3, new HashSet<EnvironmentDeclatation.PathElement<String>>(set));
    _log.toStream(System.out); _log.clear();

    _log.logInfo("Final state of the environment:");
    _log.indent();_log.logInfo(env.toString());_log.dedent();
    _log.dedent();
    _log.logInfo("Test Environment: test 2 finished");
    _log.toStream(System.out); _log.clear();
  }

  @Test
  public void test3() throws Exception {
    initDefinitions();
    initPaths();

    EnvironmentDeclatation<String, Definition> env = new EnvironmentDeclatation<>();
    env.addSearchPath(EnvironmentDeclatation.emptyOpenPath);

    _log.logInfo("Test Environment: test 3 starting...");
    _log.indent();
    _log.logInfo("initial state of the environment:");
    _log.indent();_log.logInfo(env.toString());_log.dedent();

    Set<EnvironmentDeclatation.PathElement<String>> set = new HashSet<>();
    set.add(_pathElement2);
    set.add(_pathElement3);

    _log.logInfo("Adding the first definition");
    env.put(_pathElement1, _def1, new HashSet<EnvironmentDeclatation.PathElement<String>>(set));
    _log.logInfo("Adding the second definition");
    env.put(_pathElement2, _def2, null);
    _log.logInfo("Adding the third definition");
    set.remove(_pathElement2); set.remove(_pathElement3); set.add(_pathElement1);
    env.put(_pathElement3, _def3, new HashSet<EnvironmentDeclatation.PathElement<String>>(set));

    _log.logInfo("Final state of the environment:");
    _log.indent();_log.logInfo(env.toString());_log.dedent();
    _log.dedent();
    _log.logInfo("Test Environment: test 3 finished");
  }

  @Test
  public void test4() throws Exception {
    initDefinitions();
    initPaths();

    EnvironmentDeclatation<String, Definition> env = new EnvironmentDeclatation<>();
    env.addSearchPath(_pathStar1, null, true);
    env.addSearchPath(_pathElement3, "toto", true);

    _log.logInfo("Test Environment: test 4 starting...");
    _log.indent();
    _log.logInfo("initial state of the environment:");
    _log.indent();_log.logInfo(env.toString());_log.dedent();

    Set<EnvironmentDeclatation.PathElement<String>> set = new HashSet<>();
    set.add(_pathElement2);
    set.add(_pathElement3bis);
    set.add(_pathElement4);

    _log.logInfo("Adding the first definition");
    env.put(_pathElement1, _def1, new HashSet<EnvironmentDeclatation.PathElement<String>>(set));
    _log.logInfo("Adding the second definition");
    env.put(_pathElement2, _def2, null);
    _log.logInfo("Adding the third definition");
    env.put(_pathElement3, _def3, null);
    _log.logInfo("Adding the fourth definition");
    set.remove(_pathElement2); set.remove(_pathElement3bis); set.remove(_pathElement4); set.add(_pathElement1);
    env.put(_pathElement4, _def4, new HashSet<EnvironmentDeclatation.PathElement<String>>(set));

    _log.logInfo("Final state of the environment:");
    _log.indent();
    _log.logInfo(env.toString());_log.dedent();
    _log.dedent();
    _log.logInfo("Test Environment: test 4 finished");
  }
}
