/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib;

import org.gzoumix.lib.syntax.*;
import org.gzoumix.lib.syntax.stgenerator.STGenerator;
import org.junit.Test;
import org.stringtemplate.v4.NoIndentWriter;
import org.stringtemplate.v4.ST;

import java.io.*;

public class TestSTGenerator {

//  static final String expressionFileName = "src/test/resources/expression.txt";
  static final String expressionFileName = "src/test/resources/sqrt.txt";
  static final String asmGenerated = "src-gen/test/resources/sqrt.txt";



  @Test
  public void testGenerator() throws IOException {
    try {
      ANTLRHelper.ParserConstructor<ASM6502Parser> constructor = new ANTLRHelper.ParserConstructor<ASM6502Parser>(ASM6502Lexer.class, ASM6502Parser.class);
      ASM6502Parser parser = constructor.file(expressionFileName);
      ASM6502Parser.ProgContext ctx = parser.prog();

      PrintASM6502 print = new PrintASM6502();
      ST st = print.printProg(ctx);

      File tmp;
      tmp = new File(this.asmGenerated);
      tmp.getParentFile().mkdirs();
      tmp.createNewFile();

      Writer out = new OutputStreamWriter(new PrintStream(tmp));

      try {
        st.write(new NoIndentWriter(out));
        out.flush();
      } catch (IOException e) {
        e.printStackTrace();
      }


      if(!Global.log.isEmpty()) {
        System.err.println(Global.log.toString());
      }
    } catch(Exception e) {
      e.printStackTrace();
      System.err.println(Global.log.toString());
    }
  }

  /*
    @Test
  public void testGenerator() throws IOException {
    try {
      ANTLRHelper.ParserConstructor<ExpressiParser> constructor = new ANTLRHelper.ParserConstructor<ExpressionParser>(ExpressionLexer.class, ExpressionParser.class);
      ExpressionParser parser = constructor.file(expressionFileName);
      ExpressionParser.ExpressionContext ctx = parser.expression();

      PrintExpression print = new PrintExpression();
      ST st = print.printExpression(ctx);

      Writer out = new OutputStreamWriter(System.out);

      try {
        out.write("Template: ");
        st.write(new AutoIndentWriter(out));
        out.flush();
      } catch (IOException e) {
        e.printStackTrace();
      }


      System.err.println(Global.log.toString());
    } catch(Exception e) {
      e.printStackTrace();
      System.err.println(Global.log.toString());
    }
  }

  //@Test
  public void testHandWritten() throws IOException {
    try {
      ANTLRHelper.ParserConstructor<ExpressionParser> constructor = new ANTLRHelper.ParserConstructor<ExpressionParser>(ExpressionLexer.class, ExpressionParser.class);
      ExpressionParser parser = constructor.file(expressionFileName);
      ExpressionParser.ExpressionContext ctx = parser.expression();

      ExpressionVisitor visitor = new ExpressionVisitor();
      ST st = visitor.visitExpression(ctx);

      Writer out = new OutputStreamWriter(System.out);

      try {
        out.write("Template: ");
        st.write(new AutoIndentWriter(out));
        out.flush();
      } catch (IOException e) {
        e.printStackTrace();
      }


      System.err.println(Global.log.toString());
    } catch(Exception e) {
      e.printStackTrace();
      System.err.println(Global.log.toString());
    }
  }*/
}
