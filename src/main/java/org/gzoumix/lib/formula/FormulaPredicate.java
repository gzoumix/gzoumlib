/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula;


import org.gzoumix.lib.formula.visitor.IFormulaVisitor;

import java.util.Collection;

public class FormulaPredicate<I,ID> extends IFormulaImpl<I> implements IFormulaElement<I> {
  private ID id;

  public FormulaPredicate(I info, ID id) {
    super(info);
    this.id = id;
  }


  @Override
  public boolean eval(Collection truePredicates) {
    return truePredicates.contains(this.id);
  }


  @Override
  public boolean equals(Object o) {
    if(o instanceof FormulaPredicate) {
      FormulaPredicate p = (FormulaPredicate) o;
      boolean res = this.id == p.id;
      res = res || ((this.id != null) && (p.id != null) && (this.id.equals(p.id)));
      return res;
    } else { return false; }
  }

  @Override
  public int hashCode() { return (this.id != null ) ? this.id.hashCode() : 0; }

  @Override
  public void accept(IFormulaVisitor visitor) { visitor.visit(this); }

  @Override
  public String toString() { return this.id.toString(); }
}
