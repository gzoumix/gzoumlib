/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula;

import java.util.Collection;

abstract public class IFormulaImpl<I> implements IFormula<I> {
  private I info;

  public IFormulaImpl(I info) {
    this.info = info;
  }

  @Override
  public boolean eval(Collection truePredicates) {
    return false;
  }

  @Override
  public IFormula simplify() {
    return this;
  }

  @Override
  public I getInfo() {
    return this.info;
  }

  // Data used while visiting
  private Object data;
  public <D> void setData(D data) { this.data = data; }
  public <D> D getData() { return (D)this.data; }

}
