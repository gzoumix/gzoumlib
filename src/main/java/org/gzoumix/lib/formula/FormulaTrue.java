/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula;


import org.gzoumix.lib.Global;
import org.gzoumix.lib.formula.visitor.IFormulaVisitor;

import java.util.Collection;

public class FormulaTrue<I> extends IFormulaImpl<I> implements IFormulaElement<I>  {

  public FormulaTrue(I info) { super(info); }

  @Override
  public boolean eval(Collection truePredicates) {
    return true;
  }

  @Override
  public void accept(IFormulaVisitor visitor) {
    visitor.visit(this);
  }


  @Override
  public int hashCode() { return 475203495; } // a random number

  @Override
  public boolean equals(Object o) { return (o instanceof FormulaTrue); }

  @Override
  public String toString() { return Global.Formula.TRUE; }
}
