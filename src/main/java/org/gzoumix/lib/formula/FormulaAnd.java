/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula;


import org.gzoumix.lib.Global;
import org.gzoumix.lib.data.ImmutableCollection;
import org.gzoumix.lib.formula.visitor.IFormulaVisitor;

import java.util.*;


public class FormulaAnd<I> extends ImmutableCollection<IFormula> implements IFormula<I> {
  private I info;

  public FormulaAnd(I info) { this.info = info; }
  public FormulaAnd(I info, IFormula left, IFormula right) {
    this(info);
    this.add(left);
    this.add(right);
  }

  @Override
  public boolean add(IFormula formula) {
    if(formula instanceof FormulaAnd) {
      return this.content.addAll(((FormulaAnd) formula).content);
    } else { return this.content.add(formula); }
  }

  public FormulaAnd with(IFormula formula) {
    this.add(formula);
    return this;
  }

  @Override
  public boolean eval(Collection truePredicates) {
    for(IFormula f: this) {
      if(!f.eval(truePredicates)) { return false; }
    }
    return true;
  }

  @Override
  public IFormula simplify() {
    Set<IFormula> l = new HashSet<>();
    for(IFormula formula: this) {
      IFormula sformula = formula.simplify();
      if(sformula.equals(Global.Formula.FORMULA_FALSE)) { return new FormulaFalse<>(this.getInfo()); }
      else if(!sformula.equals(Global.Formula.FORMULA_TRUE)) { l.add(sformula); }
    }

    if(l.isEmpty()) { return new FormulaTrue<>(this.getInfo()); }
    FormulaAnd res = new FormulaAnd(this.getInfo());
    res.content.addAll(l);
    return res;
  }


  @Override
  public I getInfo() { return this.info; }

  @Override
  public void accept(IFormulaVisitor visitor) { visitor.visit(this); }

  @Override
  public String toString() {
    if(this.size() == 0) { return Global.Formula.TRUE; }
    else if(this.size() == 1) { return this.iterator().next().toString(); }
    else {
      String res = "(";
      Iterator<IFormula> i = this.iterator();
      while(i.hasNext()) {
        res = res + i.next().toString();
        if(i.hasNext()) { res = res + " && "; }
      }
      return res + ")";
    }
  }
}
