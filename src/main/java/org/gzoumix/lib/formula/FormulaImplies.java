/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula;

import org.gzoumix.lib.Global;
import org.gzoumix.lib.formula.visitor.IFormulaVisitor;

import java.util.Collection;

public class FormulaImplies<I> extends IFormulaImpl<I> implements IFormula<I> {
  private IFormula left;
  private IFormula right;

  public FormulaImplies(I info, IFormula left, IFormula right) {
    super(info);
    this.left = left;
    this.right = right;
  }

  public IFormula getLeft() { return this.left; }
  public IFormula getRight() { return this.right; }

  @Override
  public boolean eval(Collection truePredicates) {
    if(this.getLeft().eval(truePredicates)) {
      return this.getRight().eval(truePredicates);
    } else { return true; }
  }

  @Override
  public IFormula simplify() {
    IFormula sleft = this.getLeft().simplify();
    if(sleft.equals(Global.Formula.FORMULA_FALSE)) { return new FormulaTrue<>(this.getInfo()); }

    IFormula sright = this.getRight().simplify();
    if (sright.equals(Global.Formula.FORMULA_TRUE)) { return new FormulaTrue<>(this.getInfo()); }

    if(sleft.equals(Global.Formula.FORMULA_TRUE)) { return sright; }

    return new FormulaImplies(this.getInfo(), sleft, sright);
  }




  @Override
  public void accept(IFormulaVisitor visitor) { visitor.visit(this); }

  @Override
  public String toString() {
    return "(" + this.getLeft().toString() + " => " + this.getRight().toString() + ")";
  }
}
