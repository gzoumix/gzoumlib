/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula;

import org.gzoumix.lib.Global;
import org.gzoumix.lib.formula.visitor.IFormulaVisitor;

import java.util.Collection;

public class FormulaNot<I> extends IFormulaImpl<I> implements IFormula<I> {
  private IFormula formula;

  public FormulaNot(I info, IFormula formula) {
    super(info);
    this.formula = formula;
  }

  public IFormula getFormula() { return this.formula; }

  @Override
  public boolean eval(Collection truePredicates) {
    return !this.formula.eval(truePredicates);
  }

  @Override
  public IFormula<I> simplify() {
    IFormula sformula = this.formula.simplify();
    if(sformula.equals(Global.Formula.FORMULA_FALSE)) { return new FormulaTrue<I>(this.getInfo()); }
    if(sformula.equals(Global.Formula.FORMULA_TRUE)) { return new FormulaFalse<I>(this.getInfo()); }

    return new FormulaNot(this.getInfo(), sformula);
  }

  @Override
  public void accept(IFormulaVisitor visitor) { visitor.visit(this); }

  @Override
  public String toString() { return Global.Formula.NOT + this.getFormula().toString(); }
}
