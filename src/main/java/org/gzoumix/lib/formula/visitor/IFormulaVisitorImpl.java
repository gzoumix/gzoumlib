/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula.visitor;

import org.gzoumix.lib.formula.*;

public class IFormulaVisitorImpl implements IFormulaVisitor {

  @Override
  public <I> void visit(FormulaTrue<I> formulaTrue) { }

  @Override
  public <I, ID> void visit(FormulaPredicate<I, ID> formulaPredicate) { }

  @Override
  public <I> void visit(FormulaAnd<I> formulaAnd) {
    for(IFormula formula: formulaAnd) { formula.accept(this); }
  }

  @Override
  public <I> void visit(FormulaEquivalent<I> formulaEquivalent) {
    formulaEquivalent.getLeft().accept(this);
    formulaEquivalent.getRight().accept(this);
  }

  @Override
  public <I> void visit(FormulaFalse<I> iFormulaFalse) { }

  @Override
  public <I> void visit(FormulaImplies<I> formulaImplies) {
    formulaImplies.getLeft().accept(this);
    formulaImplies.getRight().accept(this);
  }

  @Override
  public <I> void visit(FormulaNot<I> formulaNot) {
    formulaNot.getFormula().accept(this);
  }

  @Override
  public <I> void visit(FormulaOr<I> formulaOr) {
    for(IFormula formula: formulaOr) { formula.accept(this); }
  }
}
