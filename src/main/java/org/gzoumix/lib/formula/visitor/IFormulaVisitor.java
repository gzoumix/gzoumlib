
/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.formula.visitor;

import org.gzoumix.lib.formula.*;

public interface IFormulaVisitor {


  <I> void visit(FormulaTrue<I> formulaTrue);

  <I, ID> void visit(FormulaPredicate<I, ID> formulaPredicate);

  <I> void visit(FormulaAnd<I> formulaAnd);

  <I> void visit(FormulaEquivalent<I> formulaEquivalent);

  <I> void visit(FormulaFalse<I> iFormulaFalse);

  <I> void visit(FormulaImplies<I> formulaImplies);

  <I> void visit(FormulaNot<I> formulaNot);

  <I> void visit(FormulaOr<I> formulaOr);
}
