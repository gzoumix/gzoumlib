/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.data;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;


/**
 * Logging class that works also with concurrent threads.
 * This logging class features logging levels and indentations scopes
 */
public class Log {


  /**
   * Enum for the different level of logging
   */
  public enum LogLevel {
    OFF  ("[OFF]   "),   // The highest possible rank and is intended to turn off logging.
    FATAL("[FATAL] "),   // Severe errors that cause premature termination. Expect these to be immediately visible on a status console.
    ERROR("[ERROR] "),   // Other runtime errors or unexpected conditions. Expect these to be immediately visible on a status console.
    WARN ("[WARN]  "),   // Use of deprecated APIs, poor use of API, 'almost' errors, other runtime situations that are undesirable or unexpected, but not necessarily "wrong". Expect these to be immediately visible on a status console.
    INFO ("[INFO]  "),   // Interesting runtime events (startup/shutdown). Expect these to be immediately visible on a console, so be conservative and keep to a minimum.
    DEBUG("[DEBUG] "),   // Detailed information on the flow through the system. Expect these to be written to logs only.
    TRACE("[TRACE] ");   // Most detailed information. Expect these to be written to logs only

    /**
     * @param s Name of the Logging level
     */
    LogLevel(String s) { this.s = s; }

    private String s;

    @Override
    public String toString() { return this.s; }
  }


  /////////////////////////////////////////////
  // 2. Message classes (store extra informations for formatting)

  /**
   * Class for logged message.
   * Stores the message, the level in which it has been logged and its indentation scope.
   */
  public static class LoggedMessage {
    private LogLevel level;
    private Object message;
    private int indent;

    /**
     * Constructor for logged messages
     * @param level the logging level of the message
     * @param indent the indentation scope of the message
     * @param message the message
     */
    public LoggedMessage(LogLevel level, int indent, Object message) {
      this.level   = level;
      this.indent  = indent;
      this.message = message;
    }

    public LogLevel getLevel() { return level; }
    public Object getMessage() { return message; }
    public int getIndent() { return indent; }

    @Override
    public String toString() {
      String indentString = this.level.toString();
      for (int i = 0; i < this.indent; i++) {
        indentString += "  ";
      }
      return  indentString + this.message.toString().replace("\n", "\n" + indentString);
    }
  }

  /////////////////////////////////////////////
  // 3. Class definition
  private Queue<LoggedMessage> log;
  private int indent;
  private LogLevel outputLevel;
  private Boolean has[];

  public Log() {
    this.log = new ConcurrentLinkedDeque<>();
    this.indent = 0;
    this.outputLevel = LogLevel.TRACE;
    this.has = new Boolean[LogLevel.values().length];
    this.resetHas();
  }

  /**
   * sets the output level of this log object
   * @param level the new output level of this log object
   */
  public void setLogLevel(LogLevel level) { this.outputLevel = level; }

  /**
   * Returns the current output level of this log object
   * @return the current output level of this log object
   */
  public LogLevel getCurrentLogLevel() { return this.outputLevel; }


  /**
   * Main logging method
   * @param level the level in which to log the message
   * @param message the logged message
   */
  public void log(LogLevel level, Object message) {
    this.log.offer(new LoggedMessage(level, this.indent, message));
    this.has[level.ordinal()] = true;
    //for(int i = level.ordinal(); i < this.has.length; i++) { this.has[i] = true; }
  }

  public void logFatal(Object message) { this.log(LogLevel.FATAL, message); }
  public void logError(Object message) { this.log(LogLevel.ERROR, message); }
  public void logWarn (Object message) { this.log(LogLevel.WARN , message); }
  public void logInfo (Object message) { this.log(LogLevel.INFO , message); }
  public void logDebug(Object message) { this.log(LogLevel.DEBUG, message); }
  public void logTrace(Object message) { this.log(LogLevel.TRACE, message); }


  /**
   * Adds a new indentation scope
   */
  public void indent() { this.indent++; }

  /**
   * Remove one indentation scope
   */
  public void dedent() { if(this.indent != 0) { this.indent--; }}

  /**
   * @return if there are no logged message
   */
  public boolean isEmpty() { return this.log.isEmpty(); }

  /**
   * clear the list of logged messages
   * @return the previous logged messages
   */
  public synchronized Queue<LoggedMessage> clear() {
    Queue<LoggedMessage> res = this.log;
    this.log =  new ConcurrentLinkedDeque<>();
    this.resetHas();
    return res;
  }
  private void resetHas() {
    for(int i = 0; i < this.has.length; i++) { this.has[i] = false; }
  }

  // Basic Lookup
  public boolean has(LogLevel level) { return this.has[level.ordinal()]; }
  public boolean hasFatal() { return this.has(LogLevel.FATAL); }
  public boolean hasError() { return this.has(LogLevel.ERROR); }
  public boolean hasWarn () { return this.has(LogLevel.WARN); }
  public boolean hasInfo () { return this.has(LogLevel.INFO); }
  public boolean hasDebug() { return this.has(LogLevel.DEBUG); }
  public boolean hasTrace() { return this.has(LogLevel.TRACE); }


  /**
   * prints the logged message wth a level higher to the one set to a string
   * @return the string containing all the message with a level higher than the one of this Log object
   */
  @Override
  public String toString() {
    String res = "";
    for(LoggedMessage mes: this.log) {
      if(mes.level.ordinal() <= this.outputLevel.ordinal()) {
        res += mes.toString() + "\n";
      }
    }
    return res;
  }


  /**
   * prints the logged message wth a level higher to the one set to an output stream
   *
   * @param stream the stream where the messages are outputed to
   * @throws IOException if an I/O error occurs. In particular, an IOException is thrown if the output stream is closed.
   */
  public void toStream(OutputStream stream) throws IOException {
    for(LoggedMessage mes: this.log) {
      if(mes.level.ordinal() <= this.outputLevel.ordinal()) {
        stream.write((mes.toString() + "\n").getBytes());
      }
    }
  }

}
