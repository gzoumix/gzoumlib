/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.data;

import java.lang.reflect.Array;
import java.util.*;

/**
 * This class wraps a {@link ArrayQueue} in a list to make it extensible like a general implementation of a queue.
 * This is so the hard capacity limit of a {@link ArrayQueue} wont be so hard.
 *
 * In this implementation, no inner {@link ArrayQueue} is ever collected by the GC: empty {@link ArrayQueue} are kept in a pool so they can be reused later.
 * @param <E> the type of elements held in this collection
 */
public class ArrayQueueExtensible<E> implements Queue<E> {


  private final int capacity;
  private List<ArrayQueue<E>> content;
  private List<ArrayQueue<E>> ppool;

  /**
   * Create a {@link ArrayQueueExtensible} and sets the maximum capacity of the inner pools.
   * @param capacity the maximum capacity of the inner queue
   */
  public ArrayQueueExtensible(int capacity) {
    this.capacity = capacity;

    this.content = new ArrayList<>();
    this.ppool = new ArrayList<>();
  }

  // Implementation of the Queue interface

  @Override
  public int size() {
    int res = 0;
    for (ArrayQueue<E> pool : content) {
      res = res + pool.size();
    }
    return res;
  }

  public boolean isEmpty() {
    return this.content.isEmpty();
  }
  // no notion of full, as it is extensible

  @Override
  public boolean add(E e) {
    if (!this.offer(e)) {
      throw new IllegalStateException();
    } else {
      return true;
    }
  }


  @Override
  public boolean offer(E e) {
    if (this.isEmpty() || this.content.get(0).isFull()) { // need to add a poll
      if (this.ppool.isEmpty()) {
        this.content.add(0, new ArrayQueue<E>(this.capacity));
      } else {
        this.content.add(0, this.ppool.remove(0));
      }
    }
    this.content.get(0).offer(e);
    return true;
  }

  @Override
  public E element() {
    if(this.isEmpty()) { throw new NoSuchElementException(); }
    else { return this.peek(); }
  }

  @Override
  public E peek() { return this.isEmpty() ? null : this.content.get(this.content.size() -1).peek(); }


  @Override
  public E poll() {
    if(this.isEmpty()) { return null; }
    ArrayQueue<E> pool = this.content.get(this.content.size() - 1);
    E res = pool.poll();
    if(pool.isEmpty()) {
      this.content.remove(this.content.size() - 1);
      this.ppool.add(0, pool);
    }
    return res;
  }

  @Override
  public E remove() {
    if(this.isEmpty()) { return null; }
    ArrayQueue<E> pool = this.content.get(this.content.size() - 1);
    E res = pool.remove();
    if(pool.isEmpty()) {
      this.content.remove(this.content.size() - 1);
      this.ppool.add(0, pool);
    }
    return res;
  }

  // Implementation of the Collection Interface

  private static class ArrayQueueExtensibleIterator<E> implements Iterator<E> {

    Iterator<ArrayQueue<E>> i1;
    Iterator<E> i2;

    ArrayQueueExtensibleIterator(ArrayQueueExtensible<E> pool) {
      this.i1 = pool.content.iterator();
      if(this.i1.hasNext()) {
        this.i2 = this.i1.next().iterator();
      } else {
        this.i2 = null;
      }
    }

    @Override
    public boolean hasNext() {
      return (this.i2 != null);
    }

    @Override
    public E next() {
      E res = this.i2.next();
      if(!this.i2.hasNext()) {
        if(this.i1.hasNext()) {
          this.i2 = this.i1.next().iterator();
        } else {
          this.i2 = null;
        }
      }
      return res;
    }

    @Override
    public void remove() { throw new UnsupportedOperationException(); }
  }


  @Override
  public boolean contains(Object o) {
    Iterator<E> i = this.iterator();
    while(i.hasNext()) {
      E ob = i.next();
      if(ob.equals(o)) {return true; }
    }
    return false;
  }

  @Override
  public Iterator<E> iterator() {
    return new ArrayQueueExtensibleIterator<>(this);
  }

  @Override
  public Object[] toArray() {
    Object[] res = new Object[this.size()];
    Iterator<E> i = this.iterator();
    int id = 0;
    while(i.hasNext()) {
      res[id++] = i.next();
    }
    return res;
  }

  @Override
  public <T> T[] toArray(T[] a) {
    T[] res;
    if(a.length < this.size()) {
      res = (T[]) Array.newInstance(a.getClass().getComponentType(), this.size());
    } else { res = a; }

    Iterator<E> i = this.iterator();
    int id = 0;
    while(i.hasNext()) { res[id++] = (T)i.next(); }
    while(id < res.length) { res[id++] = null; }

    return res;
  }

  @Override
  public boolean remove(Object o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    Iterator<E> i = this.iterator();
    while(i.hasNext()) {
      if(!c.contains(i.next())) { return false; }
    }
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    for(E el: c) {
      if(!this.offer(el)) { return false; }
    }
    return true;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    for(ArrayQueue<E> pool: this.content) {
      pool.clear();
    }
    this.ppool.addAll(this.content);
    this.content.clear();
  }
}
