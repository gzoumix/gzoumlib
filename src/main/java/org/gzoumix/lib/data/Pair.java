/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.data;

/**
 * Implementation of pairs (due to Java not wanting to have such container in their STL).
 * This implementation is immutable (the elements in a pair cannot be changed)
 * @param <A> the type of the first element of the pair
 * @param <B> the type of the second element of the pair
 */

public class Pair<A, B> {
  private A first;
  private B second;

  /**
   * Constructs a pair with two elements
   * @param first the first element of the pair
   * @param second the second element of the pair
   */
  public Pair(A first, B second) {
    this.first = first;
    this.second = second;
  }

  /**
   * Getter for the first element of the pair
   * @return the first element of the pair
   */
  public A getFirst() { return this.first; }

  /**
   * Getter for the second element of the pair
   * @return the second element of the pair
   */
  public B getSecond() { return this.second; }

  public void setFirst(A first) { this.first = first; }
  public void setSecond(B second) { this.second = second; }


  @Override
  public int hashCode() {
    int hashFirst = first != null ? first.hashCode() : 0;
    int hashSecond = second != null ? second.hashCode() : 0;

    return (hashFirst + hashSecond) * hashSecond + hashFirst;
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof Pair) {
      Pair otherPair = (Pair) other;
      return
              ((  this.first == otherPair.first ||
                      ( this.first != null && otherPair.first != null &&
                              this.first.equals(otherPair.first))) &&
                      (	this.second == otherPair.second ||
                              ( this.second != null && otherPair.second != null &&
                                      this.second.equals(otherPair.second))) );
    }

    return false;
  }

  @Override
  public String toString() { return "(" + first + ", " + second + ")"; }

}