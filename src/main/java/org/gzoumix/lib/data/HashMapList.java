/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.data;

import java.util.*;

/**
 * This class implements a mapping from keys to lists of elements.
 * It adds two methods to the classic Map implementation:
 *   <ul>
 *     <li><code>putEl</code>: adds an element to a list (and creates the list if it didn't exist)</li>
 *     <li><code>putAll</code>: that merges the lists of the map in parameter to the lists in <code>this</code></li>
 *   </ul>
 *
 * @param <K> The type of keys
 * @param <V> THe type of elements in the lists
 */

public class HashMapList<K,V> extends HashMap<K,List<V>> {

  public HashMapList() { super(); }
  public HashMapList(int initialCapacity) { super(initialCapacity); }
  public HashMapList(int initialCapacity, float loadFactor) { super(initialCapacity, loadFactor); }
  public HashMapList(Map<? extends K, ? extends List<V>> m) { super(m); }

  /**
   * Put one element in the list associated to the key
   * @param key key with which the specified value is to be associated
   * @param value value to bbe associated with the specified key
   * @return the list of values associated with the key (including the added one)
   */
  public List<V> putEl(K key, V value) {
    List<V> values = this.get(key);
    if(values == null) {
      values = new ArrayList<>();
      this.put(key, values);
    }
    values.add(value);
    return values;
  }

  /**
   * Copies all the mappings from the specified map to this map while merging the mappings in common
   * @param m mappings to be stored in this map
   */
  @Override
  public void putAll(Map<? extends K,? extends List<V>> m) {
    for(Map.Entry<? extends K,? extends List<V>> entry: m.entrySet()) {
      List<V> list = this.get(entry.getKey());
      if(list != null) { list.addAll(entry.getValue()); }
      else { this.put(entry.getKey(), new ArrayList<V>(entry.getValue())); }
    }
  }
}
