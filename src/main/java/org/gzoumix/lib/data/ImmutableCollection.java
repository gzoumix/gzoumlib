/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


/**
 * This class implements a collection that cannot be modified (I didn't find such a class in the Java STL)
 * @param <E> the type of elements in this collection
 */

public class ImmutableCollection<E> implements Collection<E> {
  protected Collection<E> content;

  public ImmutableCollection() { content = new ArrayList<>(); }
  public ImmutableCollection(Collection<? extends E> c) {
    content = new ArrayList<>(c);
  }

  @Override
  public int size() { return this.content.size(); }

  @Override
  public boolean isEmpty() { return this.content.isEmpty(); }

  @Override
  public boolean contains(Object o) { return this.content.contains(o); }

  @Override
  public Iterator<E> iterator() { return new ImmutableIterator<>(this.content.iterator()); }

  @Override
  public Object[] toArray() { return this.content.toArray(); }

  @Override
  public <T> T[] toArray(T[] a) { return this.content.toArray(a); }

  @Override
  public boolean add(E v) { throw new UnsupportedOperationException(); }

  @Override
  public boolean remove(Object o) { throw new UnsupportedOperationException(); }

  @Override
  public boolean containsAll(Collection<?> c) { return this.content.containsAll(c); }

  @Override
  public boolean addAll(Collection<? extends E> c) { throw new UnsupportedOperationException(); }

  @Override
  public boolean removeAll(Collection<?> c) { throw new UnsupportedOperationException(); }

  @Override
  public boolean retainAll(Collection<?> c) { throw new UnsupportedOperationException(); }

  @Override
  public void clear() {throw new UnsupportedOperationException(); }

  @Override
  public String toString() { return this.content.toString(); }

  @Override
  public int hashCode() { return this.content.hashCode(); }

  @Override
  public boolean equals(Object o) {
    if(o instanceof ImmutableCollection) {
      return this.content.equals(((ImmutableCollection) o).content);
    } else { return false; }
  }
}
