/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.data;

/**
 * Implementation of triplets (due to Java not wanting to have such container in their STL).
 * This implementation is immutable (the elements in a triplet cannot be changed)
 * @param <A> the type of the first element of the triplet
 * @param <B> the type of the second element of the triplet
 * @param <C> the type of the third element of the triplet
 */
public class Triplet<A, B, C> {
  private A first;
  private B second;
  private C third;

  /**
   * Construct a triplet with three elements
   * @param first the first element of the triplet
   * @param second the second element of the triplet
   * @param third the third element of the triplet
   */
  public Triplet(A first, B second, C third) {
    super();
    this.first = first;
    this.second = second;
    this.third = third;
  }


  /**
   * Getter for the first element of the triplet
   * @return the first element of the triplet
   */
  public A getFirst() { return this.first; }

  /**
   * Getter for the second element of the triplet
   * @return the second element of the triplet
   */
  public B getSecond() { return this.second; }

  /**
   * Getter for the third element of the triplet
   * @return the third element of the triplet
   */
  public C getThird() { return this.third; }

  //public void setFirst(A first) { this.first = first; }
  //public void setSecond(B second) { this.second = second; }


  @Override
  public int hashCode() {
    int hashFirst = this.first != null ? this.first.hashCode() : 0;
    int hashSecond = this.second != null ? this.second.hashCode() : 0;
    int hashThird = this.third != null ? this.third.hashCode() : 0;

    return (hashFirst + hashSecond + hashThird) * hashSecond + hashFirst - hashThird;
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof Triplet) {
      Triplet otherPair = (Triplet) other;
      return (( this.first == otherPair.first) ||
                   ( this.first != null && otherPair.first != null && this.first.equals(otherPair.first)))
          && (( this.second == otherPair.second) ||
                        ( this.second != null && otherPair.second != null && this.second.equals(otherPair.second)))
          && (( this.third == otherPair.third) ||
                        ( this.third != null && otherPair.third != null && this.third.equals(otherPair.third))) ;
    } else { return false; }
  }

  @Override
  public String toString() { return "(" + this.first + ", " + this.second + ", " + this.third + ")"; }
}
