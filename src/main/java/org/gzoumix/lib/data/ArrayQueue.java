/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.data;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;


/**
 * This class implements a moving queue on top of an array.
 * This is useful for efficient memory management when the upper bound of the queue is known
 * @param <E> the type of elements held in this collection
 */
public class ArrayQueue<E> implements Queue<E> {

  private Object array[];
  private int begin;
  private int end;

  /**
   * Construct an empty queue with a maximum capacity
   * @param capacity the maximum capacity of the queue
   */
  public ArrayQueue(int capacity) {
    this.array = new Object[capacity]; //(E[]) Array.newInstance(c, capacity);
    this.begin = 0;
    this.end = 0;
  }

  /**
   * returns the capacity of the queue
   * @return the capacity of the queue
   */
  public int capacity() { return this.array.length; }

  // Implementation of the Queue interface

  @Override
  public int size() {
    int val = this.end - this.begin;
    if(val == 0) {
       return (this.array[this.begin] == null) ? 0 : this.array.length;
    } else { return val > 0 ? val : this.capacity() - this.begin + this.end; }
  }

  @Override
  public boolean isEmpty() { return (this.begin == this.end) && (this.array[this.begin] == null); }

  /**
   * returns if the queue is full
   * @return if the queue is full
   */
  public boolean isFull() { return (this.begin == this.end) && (this.array[this.begin] != null); }

  @Override
  public boolean add(E e) {
    if(!this.offer(e)) { throw new IllegalStateException(); }
    else { return true; }
  }

  @Override
  public boolean offer(E e) {
    if(this.isFull()) { return false; }
    this.array[this.end++] = e;
    if(this.end == this.array.length) { this.end = 0; }
    return true;
  }

  @Override
  public E element() {
    if(this.isEmpty()) { throw new NoSuchElementException(); }
    else { return this.peek(); }
  }

  @Override
  public E peek() { return this.isEmpty() ? null : (E)this.array[this.begin]; }

  @Override
  public E poll() {
    if(this.isEmpty()) { return null; }
    E res = this.peek();
    this.array[this.begin] = null;
    if(++this.begin == this.array.length) { this.begin = 0; }
    return res;
  }

  @Override
  public E remove() {
    E res = this.element();
    if(res != null) {
      this.array[this.begin] = null;
      if(++this.begin == this.array.length) { this.begin = 0; }
    }
    return res;
  }


  // Implementation of the Collection Interface

  private static class ArrayQueueIterator<E> implements Iterator<E> {

    int position;
    ArrayQueue<E> pool;

    ArrayQueueIterator(ArrayQueue<E> pool) {
      this.pool = pool;
      this.position = pool.begin;
    }

    @Override
    public boolean hasNext() {
      return (this.position <= this.pool.end) && (this.pool.array[this.position] != null);
    }

    @Override
    public E next() {
      E res = (E)this.pool.array[this.position++];
      if(this.position == this.pool.array.length) { this.position = 0; }
      return res;
    }

    @Override
    public void remove() { throw new UnsupportedOperationException(); }
  }


  @Override
  public boolean contains(Object o) {
    Iterator<E> i = this.iterator();
    while(i.hasNext()) {
      E ob = i.next();
      if(ob.equals(o)) {return true; }
    }
    return false;
  }

  @Override
  public Iterator<E> iterator() {
    return new ArrayQueueIterator<>(this);
  }

  @Override
  public Object[] toArray() {
    if (this.begin != 0) {
      Object tmp;
      int i = 0;
      if(this.begin < this.end) { this.end = this.end - this.begin; }
      else { this.end = this.end + this.begin - this.array.length; }

      while(this.begin < this.array.length) {
        tmp = this.array[i];
        this.array[i] = this.array[this.begin];
        this.array[this.begin++] = tmp;
      }
      this.begin = 0;
    }
    return this.array;
  }

  @Override
  public <T> T[] toArray(T[] a) {
    T[] res;
    if(a.length < this.size()) {
      res = (T[]) Array.newInstance(a.getClass().getComponentType(), this.size());
    } else { res = a; }

    Iterator<E> i = this.iterator();
    int id = 0;
    while(i.hasNext()) { res[id++] = (T)i.next(); }
    while(id < res.length) { res[id++] = null; }

    return res;
  }

  @Override
  public boolean remove(Object o) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    Iterator<E> i = this.iterator();
    while(i.hasNext()) {
      if(!c.contains(i.next())) { return false; }
    }
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    for(E el: c) {
      if(!this.offer(el)) { return false; }
    }
    return true;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    this.begin = this.end = 0;
    this.array[0] = null;
  }
}

