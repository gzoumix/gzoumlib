/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.update.model;


public class AttributeException extends ContainerGetterException {

  public AttributeException(Object attribute, Object container) {
    super();
    this.attribute = attribute;
    this.container = container;
  }


  public Object getAttribute() { return attribute; }
  public Object getContainer() { return container; }

  @Override
  public String getLocalizedMessage() {
    return super.getLocalizedMessage() + "\n\tAttribute \"" + this.attribute + "\" unknown in container \"" + this.container + "\"\n";
  }

  private Object attribute;
  private Object container;
}
