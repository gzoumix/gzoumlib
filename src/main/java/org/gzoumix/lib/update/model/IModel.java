/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.update.model;


import org.gzoumix.lib.data.Pair;

import java.util.Collection;


/**
 * This interface presents the API any updatable data must have
 * @param <C> This is the type of component in this model
 * @param <P> This is the type of port used in this model
 */
public interface IModel<C, P> {

  /**
   * Utility enum to distinguish between the two type of containers in this model
   */
  enum Kind {
    COMPONENT("component"),
    PORT("port");

    private String name;
    private Kind(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
  }

  public interface IAttribute {}
  public interface IFilter extends Collection<Pair<IAttribute,Object>> {}

  /**
   * @return the entry point of the model
   */
  P getEntryPort();

  /**
   * This method associates a Step to the output name of a port.
   * The port thus identifies the name to resolve, but a visibility object is required to actually perform the resolution
   * By hypothesis, for a given port and a given visibility object, there can be at most one component C1 visible.
   * If no component with the given name is visible, the method returns <code>null</code>
   * @param port the port specifying the name to resolve, and the component from which to perform the resolution
   * @return the pair of the component corresponding to the port and the visibility associated to it
   */
  C resolve(P port);

  /**
   * This method returns the ports of the specified component that validates the given filter
   * @param component the component containing the ports
   * @param filter the filter validated by the ports
   * @return the ports of the specified component that validate the given filter
   */
  Collection<P> getPorts(C component, IFilter filter) throws AttributeException;


  /**
   * Filtering method on a set of components
   * @param components the set of component to filter
   * @param filter the filter selecting the components that contain the specified attribute with the associated value (if the value in the pattern is null, then the value of the attributeis not constrained)
   * @return the set of components that validates the filter
   */
  Collection<C> getComponents(Collection<C> components, IFilter filter) throws AttributeException;




  // Methods to modify the component structure


  void bind(P port, C component);
  void unbind(P port);

  /**
   * Adds the component to the model, referenced with the given port
   * @param component the component to add
   * @param port the port on which this component will be bound
   */
  void adds(C component, P port);
  void removes(C component, P port);


}
