/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.update;

import org.gzoumix.lib.update.model.IModel;
import org.gzoumix.lib.update.model.UpdateException;

/**
 * This interface describes the modification API of our model.
 * Where IModel presents the basic structure of the data we manipulate and a set of small set of method for updating this structure,
 *  this interface is a generic n external specification of operation on models.
 * @param <C> the type of component
 * @param <P> the type of port
 */
public interface IModelUpdate<C, P> {

  /**
   * This method applies the update on the given model
   * @param model the model to update
   * @throws UpdateException if an error occurred during the update
   */
  void apply(IModel<C,P> model) throws UpdateException;

}
