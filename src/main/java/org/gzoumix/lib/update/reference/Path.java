/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.update.reference;

import org.gzoumix.lib.data.Pair;
import org.gzoumix.lib.update.model.ContainerGetterException;
import org.gzoumix.lib.update.model.IModel.IFilter;
import org.gzoumix.lib.update.model.IModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;


/**
 * This class allows for the creation of "abstract paths" that can identify "containers" in our update API.
 * A model in our API is structured as componentFilter that refer to each other trough portFilter: componentFilter and portFilter are both called "containers" characterized by valued attributes.
 * Here, we construct our abstract paths by specifying the attributes and their values that must be contained by the different containers in the origin.
 */
public class Path {

  public static class Root implements IReference {

    @Override
    public <C, P> Collection<P> getPorts(IModel<C, P> model) {
      P port = model.getEntryPort();
      Collection<P> res = new ArrayList<P>(1);
      res.add(port);
      return res;
    }

    @Override
    public <C, P> Collection<C> getComponents(IModel<C, P> model) throws ContainerGetterException {
      throw new ContainerGetterException("Wrong origin length");
    }

    @Override
    public <C, P, E> Collection<P> getPorts(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
      Class classModelPort = model.getClass().getTypeParameters()[1].getClass();
      Class classCollectionElement = origins.getClass().getTypeParameters()[0].getClass();
      if(classModelPort.isAssignableFrom(classCollectionElement)) {
        return (Collection<P>) origins;
      } else {
        throw new ContainerGetterException("Origin containers are not ports");
      }
    }

    @Override
    public <C, P, E> Collection<C> getComponents(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
      Class classModelComponent = model.getClass().getTypeParameters()[0].getClass();
      Class classCollectionElement = origins.getClass().getTypeParameters()[0].getClass();
      if(classModelComponent.isAssignableFrom(classCollectionElement)) {
        return (Collection<C>) origins;
      } else {
        throw new ContainerGetterException("Origin containers are not components");
      }
    }
  }

  /**
   * This class represents paths that refer to componentFilter.
   * Such origin can either be
   * <ul>
   *   <li>a subset of the roots of the model</li>
   *   <li>or a origin leading to a set of portFilter, and a filter selecting a subset of the componentFilter pointed by the portFilter</li>
   * </ul>
   * Note that the origin leading to the portFilter can either be a precise origin, or a <em>star</em> origin of arbitrary length
   */
  public static class Step implements IReference {

    /**
     * Constructor for a complex origin
     * @param origin the origin leading to a set of portFilter
     * @param filter the filter selecting a subset of the componentFilter pointed by the portFilter
     */
    public Step(IReference origin, IFilter filter) {
      this.origin = origin;
      this.filter = filter;
    }

    @Override
    public <C, P> Collection<P> getPorts(IModel<C, P> model) throws ContainerGetterException {
      return getNextPorts(model, this.origin.getComponents(model), this.filter);
    }

    @Override
    public <C, P> Collection<C> getComponents(IModel<C, P> model) throws ContainerGetterException {
      return getNextComponents(model, this.origin.getPorts(model), this.filter);
    }

    @Override
    public <C, P, E> Collection<P> getPorts(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
      return getNextPorts(model, this.origin.getComponents(model, origins), this.filter);
    }

    @Override
    public <C, P, E> Collection<C> getComponents(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
      return getNextComponents(model, this.origin.getPorts(model, origins), this.filter);
    }


    static <C,P> Collection<P> getNextPorts(IModel<C, P> model, Collection<C> components, IFilter filter) throws ContainerGetterException {
      Collection<P> res = new LinkedList<P>();
      for(C component: components) {
        res.addAll(model.getPorts(component, filter));
      }
      return res;
    }

    static <C, P> Collection<C> getNextComponents(IModel<C, P> model, Collection<P> ports, IFilter filter) throws ContainerGetterException {
      Collection<C> res = new LinkedList<C>();
      for(P port: ports) {
        res.add(model.resolve(port));
      }
      return model.getComponents(res, filter);
    }

    private IReference origin;
    private IFilter filter;
  }


  /**
   * This class stands for arbitrary length origin, constructed from the Kleene star operator.
   * Such a origin is characterized by two filters, one for componentFilter and one for portFilter, and follows all the origin where componentFilter and portFilter validate their respective origin.
   */
  public static class Star implements IReference {

    /**
     * The constructor for a star origin
     * @param origin the point where the origin starts, identified by a origin to componentFilter
     * @param componentFilter the filter for componentFilter
     * @param portFilter the filter for portFilter
     */
    public Star(IReference origin, IFilter componentFilter, IFilter portFilter) {
      this.origin = origin;
      this.componentFilter = componentFilter;
      this.portFilter = portFilter;
    }


    @Override
    public <C, P> Collection<P> getPorts(IModel<C, P> model) throws ContainerGetterException {
      return this.get(model, this.origin.getComponents(model)).getSecond();
    }

    @Override
    public <C, P> Collection<C> getComponents(IModel<C, P> model) throws ContainerGetterException {
      return this.get(model, this.origin.getComponents(model)).getFirst();
    }

    @Override
    public <C, P, E> Collection<P> getPorts(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
      return this.get(model, this.origin.getComponents(model, origins)).getSecond();
    }

    @Override
    public <C, P, E> Collection<C> getComponents(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
      return this.get(model, this.origin.getComponents(model, origins)).getFirst();
    }

    /**
     * This method compute the set of componentFilter and portFilter corresponding to the origin in a model
     * @param model the model in which the origin is followed
     * @param <C> the type of component
     * @param <P> the type of port
     * @return the set of componentFilter and portFilter in <code>model</code> that validate the origin
     */
    private <C, P> Pair<Collection<C>,Collection<P>> get(IModel<C,P> model, Collection<C> origins) throws ContainerGetterException {
      Collection<C> tmpComponent = model.getComponents(origins, this.componentFilter);
      Collection<C> resComponent = new LinkedList<C>();
      Collection<P> tmpPort = Step.getNextPorts(model, tmpComponent, this.portFilter);
      Collection<P> resPort = new LinkedList<P>();

      // fix point
      while((!resComponent.containsAll(tmpComponent)) && (!resPort.containsAll(tmpPort))) {
        resComponent.addAll(tmpComponent);
        resPort.addAll(tmpPort);

        tmpComponent = Step.getNextComponents(model, tmpPort, componentFilter);
        tmpPort = Step.getNextPorts(model, tmpComponent, portFilter);
      }

      return new Pair<>(resComponent, resPort);
    }

    private IReference origin;
    private IFilter componentFilter;
    private IFilter portFilter;

  }
}
