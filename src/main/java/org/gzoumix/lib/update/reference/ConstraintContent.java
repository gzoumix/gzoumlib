/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.update.reference;

import org.gzoumix.lib.update.model.ContainerGetterException;
import org.gzoumix.lib.update.model.IModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class ConstraintContent implements IReference {

  public enum Operation {
    EQUALS, LEQ, GEQ;

    boolean test(int a, int b) {
      switch(this) {
        case EQUALS: return a == b;
        case LEQ: return a <= b;
        case GEQ: return a >= b;
      }
      return false;
    }
  }

  public ConstraintContent(IReference origin, IReference filter, IModel.Kind filterKind, Operation op, int arity) {
    this.origin = origin;
    this.filter = filter;
    this.filterKind = filterKind;
    this.op = op;
    this.arity = arity;
  }

  @Override
  public <C, P> Collection<P> getPorts(IModel<C, P> model) throws ContainerGetterException {
    return this.filterCollection(model, this.origin.getPorts(model));
  }

  @Override
  public <C, P> Collection<C> getComponents(IModel<C, P> model) throws ContainerGetterException {
    return this.filterCollection(model, this.origin.getComponents(model));
  }

  @Override
  public <C, P, E> Collection<P> getPorts(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
    return this.filterCollection(model, this.origin.getPorts(model, origins));
  }

  @Override
  public <C, P, E> Collection<C> getComponents(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
    return this.filterCollection(model, this.origin.getComponents(model, origins));
  }


  private <C,P,E> Collection<E> filterCollection(IModel<C, P> model, Collection<E> collection) throws ContainerGetterException {
    Collection<E> res =  new LinkedList<E>();
    for(E element: collection) {
      Collection<E> tmp = new ArrayList<E>(1);
      tmp.add(element);
      switch(this.filterKind) {
        case COMPONENT: if(this.op.test(this.filter.getComponents(model, tmp).size(), this.arity)) {
            res.add(element);
          }
          break;
        case PORT: if(this.op.test(this.filter.getPorts(model, tmp).size(), this.arity)) {
            res.add(element);
          }
          break;
      }
    }
    return res;
  }


  private IReference origin;
  private IReference filter;
  private IModel.Kind filterKind;
  private Operation op;
  private int arity;
}
