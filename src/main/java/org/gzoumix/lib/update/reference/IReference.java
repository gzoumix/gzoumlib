/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.update.reference;

import org.gzoumix.lib.update.model.ContainerGetterException;
import org.gzoumix.lib.update.model.IModel;

import java.util.Collection;

public interface IReference {

  <C,P> Collection<P> getPorts(IModel<C,P> model) throws ContainerGetterException;
  <C,P> Collection<C> getComponents(IModel<C,P> model) throws ContainerGetterException;

  <C,P, E> Collection<P> getPorts(IModel<C,P> model, Collection<E> origins) throws ContainerGetterException;
  <C,P, E> Collection<C> getComponents(IModel<C,P> model, Collection<E> origins) throws ContainerGetterException;
}
