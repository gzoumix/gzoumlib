/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.update.reference;

import org.gzoumix.lib.update.model.ContainerGetterException;
import org.gzoumix.lib.update.model.IModel;

import java.util.Collection;
import java.util.LinkedList;

public class Combination implements IReference {
  public enum Operation {
    UNION, INTER, DIFF;

    public <E> Collection<E> apply(Collection<E> a, Collection<E> b) {
      Collection res;
      switch(this) {
        case UNION:
          res = new LinkedList<E>(a);
          res.addAll(b);
          return res;
        case INTER:
          res = new LinkedList<E>(a);
          res.retainAll(b);
          return res;
        case DIFF:
          res = new LinkedList<E>(a);
          res.removeAll(b);
          return res;
      }
      return null;
    }
  }

  public Combination(IReference ref1, IReference ref2, Operation op) {
    this.ref1 = ref1;
    this.ref2 = ref2;
    this.op = op;
  }

  @Override
  public <C, P> Collection<P> getPorts(IModel<C, P> model) throws ContainerGetterException {
    return this.op.apply(this.ref1.getPorts(model), this.ref2.getPorts(model));
  }

  @Override
  public <C, P> Collection<C> getComponents(IModel<C, P> model) throws ContainerGetterException {
    return this.op.apply(this.ref1.getComponents(model), this.ref2.getComponents(model));
  }

  @Override
  public <C, P, E> Collection<P> getPorts(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
    return this.op.apply(this.ref1.getPorts(model, origins), this.ref2.getPorts(model, origins));
  }

  @Override
  public <C, P, E> Collection<C> getComponents(IModel<C, P> model, Collection<E> origins) throws ContainerGetterException {
    return this.op.apply(this.ref1.getComponents(model, origins), this.ref2.getComponents(model, origins));
  }

  private IReference ref1, ref2;
  private Operation op;
}
