/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph;

import org.gzoumix.lib.data.Pair;

public class Edge<V, E> {
  private E id;
  private Vertex<V, E> start;
  private Vertex<V, E> end;

  Edge(E id, Vertex<V, E> start, Vertex<V, E> end) {
    this.id = id;
    this.start = start;
    this.end = end;
  }

  public E getID() { return this.id; }

  public V getStartID() { return this.getStartVertex().getID(); }
  public Vertex<V, E> getStartVertex() { return this.start; }

  public V getEndID() { return this.getEndVertex().getID(); }
  public Vertex<V, E> getEndVertex() { return this.end; }

  void resetStart(Vertex<V,E> start) { this.start = start; }
  void resetEnd(Vertex<V,E> end) { this.end = end; }

  @Override
  public String toString() {
    return (new Pair<>(this.getID(), this.getEndVertex())).toString();
  }
}
