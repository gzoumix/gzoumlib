/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph;

import java.util.*;

public class Graph<V, E> {
  public Map<V, Vertex<V,E>> vertices;
  public Set<V> keys;
  public Set<Vertex<V,E>> values;

  public Graph() {
    this.vertices = new HashMap<>();
    this.keys = new HashSet<>();
    this.values = new HashSet<>();
  }

  public Vertex<V,E> addVertex(V id) {
    Vertex<V,E> res = this.vertices.get(id);
    if(res != null) { return res; }
    else {
      res = new Vertex<>(id);
      this.keys.add(id);
      this.values.add(res);
      this.vertices.put(id, res);
      return res;
    }
  }

  public Edge<V,E> addEdge(E id, V start, V end) {
    Vertex<V,E> startV = this.vertices.get(start);
    Vertex<V,E> endV = this.vertices.get(end);
    if((startV != null) && (endV != null)) {
      Edge<V,E> edge = new Edge<>(id,startV,endV);
      startV.addNext(edge);
      endV.addPrev(edge);
      return edge;
    } else { return null; }
  }
  public Edge<V,E> addEdge(E id, Vertex<V,E> startV, Vertex<V,E> endV) {
    if((startV != null) &&  this.values.contains(startV) && (endV != null) && this.values.contains(endV)) {
      Edge<V,E> edge = new Edge<>(id,startV,endV);
      startV.addNext(edge);
      endV.addPrev(edge);
      return edge;
    } else { return null; }
  }




  public Vertex<V,E> mergeVertex(V newID, V v1, V v2) {
    if(!newID.equals(v1) && !newID.equals(v2) && this.keys.contains(newID)) { return null; }

    Vertex<V,E> v1v = this.getVertex(v1);
    Vertex<V,E> v2v = this.getVertex(v2);

    if((v1v == null) || (v2v == null)) { return null; }

    return this.mergeVertex(newID, v1, v1v, v2, v2v);
  }

  public Vertex<V,E> mergeVertex(V newID, Vertex<V,E> v1v, Vertex<V,E> v2v) {
    if((v1v == null) || (v2v == null)) { return null; }
    if(!this.values.contains(v1v) || !this.values.contains(v2v)) { return null; }

    V v1 = v1v.getID();
    V v2 = v2v.getID();

    if(!newID.equals(v1) && !newID.equals(v2) && this.keys.contains(newID)) { return null; }

    return this.mergeVertex(newID, v1, v1v, v2, v2v);
  }

  private Vertex<V,E> mergeVertex(V newID, V v1, Vertex<V,E> v1v, V v2, Vertex<V,E> v2v) {
    this.keys.remove(v1);
    this.keys.remove(v2);
    this.vertices.remove(v1);
    this.vertices.remove(v2);
    this.values.remove(v1v);

    v2v.resetID(newID);
    this.keys.add(newID);
    this.vertices.put(newID, v2v);
    for(Edge<V,E> e: v1v.getNexts()) {
      e.resetStart(v2v);
      v2v.addNext(e);
    }
    for(Edge<V,E> e: v1v.getPrevs()) {
      e.resetEnd(v2v);
      v2v.addPrev(e);
    }
    return v2v;
  }


  public Set<Vertex<V,E>> getVertices() { return this.values; }
  public Set<V> getVertexIDs() { return this.keys; }
  public Vertex<V,E> getVertex(V v) { return this.vertices.get(v); }
  public Collection<Edge<V,E>> getNexts(V id) {
    Vertex<V,E> v = this.vertices.get(id);
    if(v == null) { return null; }
    else { return v.getNexts(); }
  }

  public Collection<Edge<V,E>> getPrevs(V id) {
    Vertex<V,E> v = this.vertices.get(id);
    if(v == null) { return null; }
    else { return v.getPrevs(); }
  }



  @Override
  public String toString() {
    String res = "";
    Iterator<Vertex<V,E>> i = this.vertices.values().iterator();
    while(i.hasNext()) {
      Vertex<V,E> v = i.next();
      res += "  " + v.toString() + " -> " + v.getNexts().toString();
      if(i.hasNext()) { res += "\n"; }
    }
    return res;
  }


  /////////////////////////////////////////////
  // 4. Search algorithms

  static final int VERTICE_NOT_VISITED_ID = -1;        // to remember the order in which vertices have been setVisitedID
  private int verticeVisitID;

  public void resetVertices() {
    for(Vertex<V,E> v: this.vertices.values()) { v.setVisitedID(VERTICE_NOT_VISITED_ID); }
    this.verticeVisitID = VERTICE_NOT_VISITED_ID;
  }

  public void visitVertex(Vertex<V,E> v) {
    v.setVisitedID(++this.verticeVisitID);
  }


}
