/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph;

import org.gzoumix.lib.data.HashMapList;

import java.util.ArrayList;
import java.util.List;

public class Vertex<V, E> {
  // 1.1. Basic Vertice Structure
  private V id;
  private List<Edge<V,E>> nexts;
  private List<Edge<V,E>> prevs;
  private HashMapList<E, Edge<V,E>> mapNexts;
  private HashMapList<E, Edge<V,E>> mapPrevs;

  public Vertex(V info) {
    this.id = info;
    this.nexts = new ArrayList<>();
    this.prevs = new ArrayList<>();
    this.mapNexts = new HashMapList<>();
    this.mapPrevs = new HashMapList<>();
    this.resetVisited();
  }

  void addNext(Edge<V, E> edge) { this.nexts.add(edge); this.mapNexts.putEl(edge.getID(), edge); }
  void addPrev(Edge<V, E> edge) { this.prevs.add(edge); this.mapPrevs.putEl(edge.getID(), edge); }

  public boolean hasNext(Vertex<V,E> v) {
    for(Edge<V,E> e: this.getNexts()) { if(e.getEndVertex().equals(v)) { return true; } }
    return false;
  }

  public V getID() { return this.id; }
  void resetID(V id) { this.id = id; }

  public List<Edge<V,E>> getNexts() { return this.nexts; }
  public List<Edge<V,E>> getPrevs() { return this.prevs; }
  public List<Edge<V,E>> getNexts(E id) { return this.mapNexts.get(id); }
  public List<Edge<V,E>> getPrevs(E id) { return this.mapPrevs.get(id); }

  // 1.2. Visitor Data
  private int visitedID;            // when this vertice is first setVisitedID
  public int getVisitedID() { return this.visitedID; }
  public boolean isVisited() { return this.visitedID != Graph.VERTICE_NOT_VISITED_ID; }

  void resetVisited() {
    this.visitedID = Graph.VERTICE_NOT_VISITED_ID;
  }
  void setVisitedID(int i) {
    this.visitedID = i;
  }

  private Object data;
  public <D> void setData(D data) { this.data = data; }
  public <D> D getData() { return (D)this.data; }


  // 1.3. Methods for hashing data structures
  @Override
  public int hashCode() {
    return this.id.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Vertex) {
      return this.id.equals(((Vertex) o).getID());
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.id.toString();
  }
}
