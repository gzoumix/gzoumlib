/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph;

import org.gzoumix.lib.data.HashMapSet;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GraphKeyed<K,V,E> extends Graph<V,E> {

  private Map<K,Vertex<V,E>> toVertices;
  private HashMapSet<Vertex<V,E>, K> toKeys;

  public GraphKeyed() {
    super();
    this.toVertices = new HashMap<>();
    this.toKeys = new HashMapSet<>();
  }

  @Override
  public Vertex<V,E> addVertex(V id) {
    throw new UnsupportedOperationException();
  }

  public Vertex<V,E> addVertex(K key, V id) {
    Vertex<V,E> res = this.toVertices.get(key);
    if(res == null) {
      res = super.addVertex(id);
      this.toVertices.put(key, res);
      this.toKeys.putEl(res, key);
    }
    return res;
  }

  public Vertex<V,E> getVertexFromKey(K key) { return this.toVertices.get(key); }

  @Override
  public Vertex<V,E> mergeVertex(V newID, V v1, V v2) {
    Vertex<V,E> v1v = this.getVertex(v1);
    Vertex<V,E> v2v = this.getVertex(v2);
    return this.mergeVertex(newID, v1v, v2v);
  }

  @Override
  public Vertex<V,E> mergeVertex(V newID, Vertex<V,E> v1v, Vertex<V,E> v2v) {
    Vertex<V,E> res = super.mergeVertex(newID, v1v, v2v);
    if(res == null) { return null; }

    Set<K> s = new HashSet<>(this.toKeys.get(v1v));
    s.addAll(this.toKeys.get(v2v));

    this.toKeys.remove(v1v);
    this.toKeys.remove(v2v);
    this.toKeys.put(res, s);

    for(K key: s) { this.toVertices.put(key, res); }

    return res;
  }
}
