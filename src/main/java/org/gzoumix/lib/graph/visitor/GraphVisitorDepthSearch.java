/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph.visitor;

import org.gzoumix.lib.graph.Edge;
import org.gzoumix.lib.graph.Graph;
import org.gzoumix.lib.graph.Vertex;


public class GraphVisitorDepthSearch<V,E> implements IGraphVisitor<V,E> {



  protected Graph<V,E> graph;

  @Override
  public void visit(Graph<V, E> graph) {
    this.graph = graph;
    this.graph.resetVertices();

    for(Vertex<V,E> v: graph.getVertices()) {
      if(!v.isVisited()) {
        this.visitDepth(v);
      }
    }
  }

  private void visitDepth(Vertex<V,E> v) {
    this.enter(v);
    this.graph.visitVertex(v);

    for(Edge<V,E> e: v.getNexts()) {       // for each vertex w such that (v; w) in E do begin
      boolean fresh = false;
      Vertex<V, E> w = e.getEndVertex();
      if (!w.isVisited()) { this.visitDepth(w); fresh = true; }
      this.step(e, fresh);
    }
    this.leave(v);
  }



  @Override
  public void enter(Vertex<V, E> v) { }

  @Override
  public void step(Edge<V, E> e, boolean fresh) { }

  @Override
  public void leave(Vertex<V, E> v) { }
}
