/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph.visitor;

import org.gzoumix.lib.graph.Edge;
import org.gzoumix.lib.graph.Graph;
import org.gzoumix.lib.graph.Vertex;

import java.util.LinkedList;


public class GraphTopologicalOrderFactory<V,E> extends GraphVisitorDepthSearch<V,E> {

  public static class LoopException extends Exception {}


  public static <V,E> LinkedList<Vertex<V,E>> create(Graph<V,E> graph) throws LoopException {
    GraphTopologicalOrderFactory factory = new GraphTopologicalOrderFactory(graph);
    if(factory.hasLoop) { throw new LoopException(); }
    return factory.get();
  }


  private boolean hasLoop;
  private LinkedList<Vertex<V,E>> res;
  private GraphTopologicalOrderFactory(Graph<V, E> graph) {
    this.res = new LinkedList<>();
    this.hasLoop = false;
    this.visit(graph);
  }

  public LinkedList<Vertex<V,E>> get() { return this.res; }


  /////////////////////////////////////////////////////////////////////////////
  // Implementation of the IGraphVisitor interface

  @Override
  public void enter(Vertex<V,E> v) { v.setData(null); }

  @Override
  public void step(Edge<V, E> e, boolean fresh) {
    if(!fresh && (e.getEndVertex().getData() == null)) { this.hasLoop = true; }
  }

  @Override
  public void leave(Vertex<V,E> v) {
    v.setData(this);
    this.res.addFirst(v);
  }
}
