/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph.visitor;

import org.gzoumix.lib.graph.Edge;
import org.gzoumix.lib.graph.Graph;
import org.gzoumix.lib.graph.Vertex;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;


public class GraphVisitorBreadthSearch<V,E> implements IGraphVisitor<V,E> {



  protected Graph<V,E> graph;

  private Queue<Vertex<V,E>> up;
  private Stack<Vertex<V,E>> down;
  protected int level;

  @Override
  public void visit(Graph<V, E> graph) {
    this.graph = graph;
    this.graph.resetVertices();

    this.up = new LinkedList<>();
    this.down = new Stack<>();
    this.level = 0;

    for(Vertex<V,E> v: graph.getVertices()) {
      if(!v.isVisited()) {
        this.up.add(v);
        this.down.push(v);
        this.up.add(null); // next level
        this.down.push(null); // next level

        // up
        while(!this.up.isEmpty()){
          v = this.up.remove();
          if((v == null) && !this.up.isEmpty()) { // next level
            this.up.add(null); // next level
            this.down.push(null); // next level
            this.level++;
          } else if(v != null) {
            this.graph.visitVertex(v);
            this.enter(v);
            for(Edge<V,E> e: v.getNexts()) {
              Vertex<V,E> w = e.getEndVertex();
              this.up.add(w);
            }
          }
        }

        // down
        while(!this.down.isEmpty()) {
          v = this.down.pop();
          if(v == null) { this.level--; }
          else {
            for(Edge<V,E> e: v.getNexts()) {
              boolean fresh = v.getVisitedID() < e.getEndVertex().getVisitedID();
              this.step(e, fresh);
            }
            this.leave(v);
          }
        }

      }
    }
  }



  @Override
  public void enter(Vertex<V, E> v) { }

  @Override
  public void step(Edge<V, E> e, boolean fresh) { }

  @Override
  public void leave(Vertex<V, E> v) { }
}
