/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph.visitor;

import org.gzoumix.lib.data.Pair;
import org.gzoumix.lib.graph.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;


public class GraphTransitiveClosureFactory<V,E> extends GraphVisitorDepthSearch<V,E> {

  public static <V,E> ComponentGraph<V,E> create(Graph<V,E> graph) {
    return (new GraphTransitiveClosureFactory(graph)).get();
  }


  private Graph<V,E> graph;
  private ComponentGraph<V,E> res;

  private Stack<Vertex<V,E>> vstack;                                     // the stack of parsed vertice
  private Stack<Pair<Edge<V,E>, ComponentGraph.Component<V,E>>> cstack;  // the stack of discovered components

  private GraphTransitiveClosureFactory(Graph<V,E> graph) {
    this.graph = graph;

    this.vstack = new Stack<>();
    this.cstack = new Stack<>();

    this.res = new ComponentGraph<>();

    this.visit(this.graph);
  }

  public ComponentGraph<V,E> get() { return this.res; }


  /////////////////////////////////////////////////////////////////////////////
  // Implementation of the IGraphVisitor interface

  private static final class Data<V,E> {
    Vertex<V, E> root;         // the root of the component
    ComponentGraph.Component<V,E> component;    // the component
    int csaved;                // the state of the cstack when setVisitedID
    boolean loop;
  }



  @Override
  public void enter(Vertex<V,E> v) {
    Data<V,E> data = new Data<>();
    data.root = v;                                       // root(v) := v;
    data.csaved = this.cstack.size();                    // csaved(v) := height(cstack);
    data.component = null;                               // C(v) := Nil;
    data.loop = false;
    this.vstack.push(v);                                 // push(v; vstack);
    v.setData(data);
  }

  @Override
  public void step(Edge<V, E> e, boolean fresh) {
    Vertex<V,E> v = e.getStartVertex();
    Vertex<V,E> w = e.getEndVertex();
    Data<V,E> datav = v.getData();
    Data<V,E> dataw = w.getData();

    if(dataw.component == null) { // found a loop
      // update the root when necessary
      if(dataw.root.getVisitedID() < datav.root.getVisitedID()) {
        datav.root = dataw.root;
      }
      System.out.println("found that " + v + " has a loop due to " + w);
      datav.loop = true;
    } else if(fresh || (w.getVisitedID() < v.getVisitedID())) {  // else if (v; w) is not a forward edge then push(C(w); cstack);
      cstack.push(new Pair<>(e, dataw.component)); // Collect all the edge between components
    }
  }

  @Override
  public void leave(Vertex<V,E> v) {
    Data<V,E> datav = v.getData();

    if(datav.root.equals(v)) {

      //System.out.print(" => creating a Step for vertex " + v.toString());

      ComponentGraph.Component<V,E> C = new ComponentGraph.Component<>();
      C.setLoop(datav.loop);


      // Set up the the Step in the graph
      Vertex<V,E> w;
      do {                      // repeat
        w = this.vstack.pop();  // w := pop(vstack)
        Data<V,E> dataw = w.getData();
        dataw.component = C;      // C(w) := C;
        C.addVertex(w); // insert w into component C;
      } while(!w.equals(v));
      //System.out.println(" = " + C.toString());

      List<Pair<List<Edge<V,E>>,ComponentGraph.Component<V,E>>> nexts = new Vector<>();

      //System.out.print("  nexts = ");
      // Constructing the set of next Components
      while(datav.csaved != this.cstack.size()) {      // while height(cstack) != csaved(v) do begin
        Pair<Edge<V,E>,ComponentGraph.Component<V,E>> X = this.cstack.pop();             // X := pop(cstack);
        if(!nexts.contains(X)) {                       // if X not in Succ(C) then
          List<Edge<V,E>> edges = new LinkedList<>();
          edges.add(X.getFirst());
          nexts.add(new Pair<>(edges, X.getSecond()));                                // Succ(C) := Succ(C) cup {X} cup Succ(X);
          //System.out.println(X.iterator().next() + ", ");
          for(Edge<ComponentGraph.Component<V,E>, List<Edge<V,E>>> e: this.res.getNexts(X.getSecond())) {
            edges = new LinkedList<>();
            edges.add(X.getFirst());
            edges.addAll(e.getID());
            //System.out.println(e.getEndID().iterator().next() + ", ");
            nexts.add(new Pair<>(edges, e.getEndID()));
          }
        }
      }

      // set up the graph
      res.addVertex(C);
      for(Pair<List<Edge<V,E>>,ComponentGraph.Component<V,E>> X: nexts) { res.addEdge(X.getFirst(), C, X.getSecond()); }
    }
  }
}
