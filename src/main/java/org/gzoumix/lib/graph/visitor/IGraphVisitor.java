/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph.visitor;

import org.gzoumix.lib.graph.Edge;
import org.gzoumix.lib.graph.Graph;
import org.gzoumix.lib.graph.Vertex;

public interface IGraphVisitor<V,E> {
  void visit(Graph<V,E> graph);
  void enter(Vertex<V,E> v);
  void step(Edge<V, E> e, boolean fresh);
  void leave(Vertex<V,E> v);
}
