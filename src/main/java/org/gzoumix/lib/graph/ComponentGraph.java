/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.graph;

import org.gzoumix.lib.data.ImmutableCollection;

import java.util.LinkedList;
import java.util.List;

public class ComponentGraph<V,E> extends Graph<ComponentGraph.Component<V,E>, List<Edge<V,E>>> {


  public Graph<V, List<Edge<V,E>>> flatten() {
    Graph<V,  List<Edge<V,E>>> res = new Graph<>();

    // 1. define the vertices of the graph
    for (Component<V,E> comp : this.getVertexIDs()) {
      for (Vertex<V,E> v : comp) {
        res.addVertex(v.getID());
      }
    }

    // 2. generate the edges form within the Components
    for (Component<V,E> comp : this.getVertexIDs()) {
      if (comp.hasLoop()) {
        for (Vertex<V,E> v : comp) {
          for (Vertex<V,E> w : comp) {
            if((!v.equals(w)) || (comp.hasLoop())) { res.addEdge(new LinkedList<Edge<V, E>>(), v.getID(), w.getID()); }
          }
        }
      }
    }

    // 3. generate the edges between the Components
    for (Component<V,E> comp1 : this.getVertexIDs()) {
      for (Edge<Component<V,E>, List<Edge<V,E>>> e : this.getNexts(comp1)) {
        for (Vertex<V,E> v : comp1) {
          for (Vertex<V,E> w : e.getEndID()) {
            res.addEdge(e.getID(), v.getID(), w.getID());
          }
        }
      }
    }
    return res;
  }



  public static class Component<V,E> extends ImmutableCollection<Vertex<V,E>> {
    private boolean hasLoop;

    public Component() { super(); this.hasLoop = false; }

    public Component(Vertex<V,E> v) {
      this();
      this.addVertex(v);
    }

    public void addVertex(Vertex<V,E> v) {
      this.content.add(v);
      if (this.size() > 1) {
        this.hasLoop = true;
      }
    }

    public boolean hasLoop() {
      return this.hasLoop;
    }

    public void setLoop(boolean b) {
      this.hasLoop = b;
    }
  }
}
