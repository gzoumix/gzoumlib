/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.solver.unification;

import org.gzoumix.lib.graph.Edge;
import org.gzoumix.lib.graph.GraphKeyed;
import org.gzoumix.lib.graph.Vertex;

import java.util.*;


/**
 * This class implements a unification algorithm between {@link ITerm}s.
 * Moreover, it constructs the full graph of unification with history to find the possible cause of an unification error.
 * This class is not thread-safe.
 *
 * @param <I> is the type of the information
 */
public class Constraint<I> {


  /**
   * This class stores the information of a vertex in the unification graph.
   * A Vertex stores a term.
   * In the special case in which the term is a variable, a vertex acts as a equivalence class, so terms coming from different variables can be unified
   */
  public static class AnnotationVertex {
    public static class Kind { // the kind of the term in the vertex: either STRUCTURED (i.e., it has a constructor), either VARIABLE
      public static final Kind STRUCTURED = new Kind();
      public static final Kind VARIABLE = new Kind();
      private Kind() {}
    }

    /**
     * public constructor for the annotation
     * @param term the term for this annotation
     * @return a vertex for the given term
     */
    public static AnnotationVertex TERM(ITerm term) {
      Kind kind = term.isVariable() ? Kind.VARIABLE : Kind.STRUCTURED;
      return new AnnotationVertex(kind, term);
    }

    private Kind kind;
    private Set<ITerm> terms;

    private AnnotationVertex(Kind kind, ITerm term) {
      this.kind = kind;
      this.terms = new HashSet<>();
      this.terms.add(term);
    }

    public Kind getKind() { return this.kind; }
    public boolean isStructured() { return this.getKind() == Kind.STRUCTURED; }
    public boolean isVariable() { return this.getKind() == Kind.VARIABLE; }
    public ITerm getStructured() { return this.terms.iterator().next(); }
    public Set<ITerm> getVariables() { return this.terms; }

    public boolean add(Set<ITerm> variables) {
      if(this.getKind() == Kind.STRUCTURED) { return false; }

      this.terms.addAll(variables);
      return true;
    }

    @Override
    public String toString() {
      return "AnnotationVertex{" + "kind=" + kind + ", terms=" + terms + '}';
    }
  }


  /**
   * This class implements the information associated to unification edges (i.e., unification constraints between two terms, stored inside vertices)
   * The information is how this constraint was computed.
   * There are three possibilities:
   * <ul>
   *   <li><code>ROOT</code>: this constraint is given by the user</li>
   *   <li><code>SUBTERM</code>: this constraint comes from forwarding an existing constraint to subterms</li>
   *   <li><code>EQUAL</code>:</li>
   * </ul>
   * @param <I> the type of user-given origin for the constraint
   */
  public static class AnnotationEdge<I> {
    public static class Kind {
      public static final Kind EQUALS = new Kind();
      public static final Kind SUBTERM = new Kind();
      public static final Kind ROOT = new Kind();
      private Kind() {}
    }

    public static <I> AnnotationEdge<I> EQUALS(Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> origin) {
      return new AnnotationEdge<I>(Kind.EQUALS, origin, null);
    }

    public static <I> AnnotationEdge<I> SUBTERM(Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> origin) {
      return new AnnotationEdge<I>(Kind.SUBTERM, origin, null);
    }

    public static <I> AnnotationEdge<I> ROOT(I info) {
      return new AnnotationEdge<I>(Kind.ROOT, null, info);
    }


    private Kind kind;                                                    // the kind saying how this edge was constructed
    private Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> origin; // all the constraints ancestor to the current edge
    private I info;                                                       // stores the information of user-created edges

    private AnnotationEdge(Kind kind, Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> origin, I info ) {
      this.kind = kind;
      this.origin = origin;
      this.info = info;
    }

    public Kind getKind() { return this.kind; }
    public boolean isRoot() { return this.getKind() == Kind.ROOT; }
    public boolean isEquals() { return this.getKind() == Kind.EQUALS; }
    public boolean isSubterm() { return this.getKind() == Kind.SUBTERM; } // these edges are actually never created
    public Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> getOrigin() { return this.origin; }
    public I getInfo() { return this.info; }
  }


  /**
   * This class stores constraints that must be checked (i.e., solved) and added to the unification graph
   * @param <I> the type of user-given origin for the constraint
   */
  private static class ConstraintEdge<I> {
    private AnnotationEdge<I> ann;
    private ITerm t1;
    private ITerm t2;

    public ConstraintEdge(AnnotationEdge<I> ann, ITerm t1, ITerm t2) {
      this.ann = ann;
      this.t1 = t1;
      this.t2 = t2;
    }

    public AnnotationEdge<I> getAnn() { return ann; }
    public ITerm getT1() { return t1; }
    public ITerm getT2() { return t2; }
  }



  private GraphKeyed<ITerm, AnnotationVertex, AnnotationEdge<I>> graph; // the unification graph
  private Queue<ConstraintEdge<I>> queue;                               // the list of constraints yet to check and add to the graph
  private Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> errors; // the set of edges that cannot be unified

  /**
   * Constructor of the class
   */
  public Constraint() {
    this.graph = new GraphKeyed<>();
    this.queue = new LinkedList<>();
    this.errors = new ArrayList<>();
  }

  /**
   * adds a constraint to this Constraint object
   * @param info the user-given origin of the constraint
   * @param t1 the first term of the unification constraint
   * @param t2 the second term of the unification constraint
   */
  public void add(I info, ITerm t1, ITerm t2) { this.add(AnnotationEdge.ROOT(info), t1, t2); }


  private void add(AnnotationEdge<I> ann, ITerm t1, ITerm t2) {
    if(t2.isVariable() && !t1.isVariable()) { ITerm tmp = t1; t1 = t2; t2 = tmp; } // no edge from structured to variable
    this.queue.add(new ConstraintEdge<I>(ann, t1, t2));
  }


  private void solve(ConstraintEdge<I> e) {
    if(this.contains(e)) { return; }

    ITerm t1 = e.getT1();
    ITerm t2 = e.getT2();

    Vertex<AnnotationVertex, AnnotationEdge<I>> v1 = this.graph.addVertex(t1, AnnotationVertex.TERM(t1)); // get a vertex for t1
    Vertex<AnnotationVertex, AnnotationEdge<I>> v2 = this.graph.addVertex(t2, AnnotationVertex.TERM(t2)); // get a vertex for t2

    /*if(v1 == null) {
      System.out.println("ERROR: failed to create node for Term1 \"" + t1 + "\"");
    } else if(v1.getNexts() == null) {
      System.out.println("ERROR: node for Term1 \"" + t1 + "\" have null edge list");
    }

    if(v2 == null) {
      System.out.println("ERROR: failed to create node for Term2 \"" + t2 + "\"");
    } else if(v2.getNexts() == null) {
      System.out.println("ERROR: node for Term2 \"" + t2 + "\" have null edge list");
    }*/


    if(t1.isVariable() && t2.isVariable()) { // merge the two vertices, and unify the images
      Iterator<Edge<AnnotationVertex, AnnotationEdge<I>>> i1= v1.getNexts().iterator();
      Iterator<Edge<AnnotationVertex, AnnotationEdge<I>>> i2= v2.getNexts().iterator();
      if(i1.hasNext() && i2.hasNext()) {
        Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> anns = new ArrayList<>();
        Edge<AnnotationVertex, AnnotationEdge<I>> e1 = i1.next();
        Edge<AnnotationVertex, AnnotationEdge<I>> e2 = i2.next();
        anns.add(e1); anns.add(e2); // anns.add(e); // pb here, because we cannot have edges between two variables.
        this.add(AnnotationEdge.EQUALS(anns), e1.getEndID().getStructured(), e2.getEndID().getStructured());
      }

      AnnotationVertex a = v1.getID();
      a.add(v2.getID().getVariables());
      this.graph.mergeVertex(a, v1, v2);

    } else if(t1.isVariable()) {
      Iterator<Edge<AnnotationVertex, AnnotationEdge<I>>> i1 = v1.getNexts().iterator();
      if(i1.hasNext()) { // if there is an image for t1, unify it
        Edge<AnnotationVertex, AnnotationEdge<I>> e2 = i1.next();
        Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> anns = new ArrayList<>();
        anns.add(e2); // anns.add(e); // pb here, because we cannot have edges between two variables.
        this.add(AnnotationEdge.EQUALS(anns), t2, e2.getEndID().getStructured());
      } else { // we found an image for the variable
        this.graph.addEdge(e.getAnn(), v1, v2);
      }
     } else {
      Edge<AnnotationVertex, AnnotationEdge<I>> edge = this.graph.addEdge(e.getAnn(), v1, v2);
      Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> anns = new ArrayList<>();
      anns.add(edge);
      AnnotationEdge<I> ann = AnnotationEdge.SUBTERM(anns);

      if (t1.getConstructor().equals(t2.getConstructor())) {
        List<ITerm> l1 = t1.getSubterms();
        List<ITerm> l2 = t2.getSubterms();
        if(l1.size() == l2.size()) {
          Iterator<ITerm> i1 = l1.iterator();
          Iterator<ITerm> i2 = l2.iterator();

          while(i1.hasNext()) {
            ITerm t1sub = i1.next();
            ITerm t2sub = i2.next();
            this.add(ann, t1sub, t2sub);
          }
        } else { this.errors.add(edge); }
      } else { this.errors.add(edge); }
    }


  }

  private boolean contains(ConstraintEdge<I> e) {
    Vertex<AnnotationVertex, AnnotationEdge<I>> v1 = this.graph.getVertexFromKey(e.getT1());
    Vertex<AnnotationVertex, AnnotationEdge<I>> v2 = this.graph.getVertexFromKey(e.getT2());

    if((v1 != null) && (v2 != null) && (v1.hasNext(v2))) { return true; }
    else { return false; }
  }


  /**
   * This adds the added edges to the unification graph, and solves them.
   * If some edges are not unifiable, they are added to an error list
   */
  public void solve() {
    while(!this.queue.isEmpty()) {
      ConstraintEdge<I> e = this.queue.poll();
      this.solve(e);
    }
  }

  /**
   * This method returns if the input constraints cannot be solved, i.e., if there are two terms that cannot be unified
   * @return if some terms could not be unified
   */
  public boolean hasError() { return this.errors.size() > 0; }

  /**
   * returns the list of edges that could not be solved. The edges contain the history of their creation, so it is possible to go back from them to the set of user-given constraint that generated this error
   * @return the list of edges that could not be solved
   */
  public Collection<Edge<AnnotationVertex, AnnotationEdge<I>>> getErrors() { return this.errors; }

  /**
   * return the vertex of a given term
   * @param t the input term
   * @return the vertex associated to the input term
   */
  public Vertex<AnnotationVertex, AnnotationEdge<I>> getVertex(ITerm t) {
    return this.graph.getVertexFromKey(t);
  }

  /**
   * returns the unification graph for this Constraint object
   * @return the unification graph for this Constraint object
   */
  public GraphKeyed<ITerm, AnnotationVertex, AnnotationEdge<I>> getGraph() { return this.graph; }
}
