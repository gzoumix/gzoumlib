
/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.solver.unification;


import java.util.List;

/**
 * This interface describes the basic structure of a term.
 * A term is a syntax tree where
 * <ul>
 *   <li>leafs are variables or construtors without parameters</li>
 *   <li>inner nodes are constructors with subterms</li>
 * </ul>
 * Note that this interface allows for recursive terms, as we have an object structure with pointers...
 *
 * @param <C> the class of the term constructor
 */

public interface ITerm<C>  {
  /**
   * Getter saying if the term is a variable
   * @return true if the term is a variable (false otherwise)
   */
  boolean isVariable();

  /**
   * Getter for the constructor of the term
   * @return the constructor of the term, or <code>null</code> if the term is a variable
   */
  C getConstructor();

  /**
   * Getter for the subterms of the term
   * @return the list of subterm of the current term, or <code>null</code> if the term is a variable
   */
  List<ITerm> getSubterms();
}
