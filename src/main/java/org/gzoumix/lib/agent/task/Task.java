/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.agent.task;


import org.gzoumix.lib.agent.runtime.RuntimeSignal;
import org.gzoumix.lib.agent.runtime.RuntimeTaskColor;
import org.gzoumix.lib.agent.runtime.RuntimeThread;

import java.util.Collection;

/**
 * This abstract class is the skeleton of a task of our runtime
 */
public abstract class Task {

  private int nbWait = 0;    // the number of signals this task is wating
  private RuntimeTaskColor color = null;       // the color of the task

  // 0. create a task in the given color
  protected Task() { }

  // 1. publishing a task

  /**
   * Insert the current task into a color, and so it can be executed by the runtime.
   * @param color the color of the task
   * @param signals the set of signals to wait before this task gets executed
   * @return if the task is active (can be executed directly) or not
   */
  public boolean publishWithColor(RuntimeTaskColor color, Collection<RuntimeSignal> signals) {
    this.color = color;
    this.nbWait = signals.size();
    color.add(this, signals);
    return (this.nbWait == 0);
  }

  /**
   * Insert the task in parameter into the current task's color.
   * Useful to specify continuations.
   * @param task the task to insert in the current task's color
   * @param signals the set of signals to wait before this task gets executed
   * @return if the inserted task is active (can be executed directly) or not
   */
  protected boolean publish(Task task, Collection<RuntimeSignal> signals) { return task.publishWithColor(this.color, signals); }

  // 3. while waiting

  /**
   * Getter for the color of the task.
   * @return the color of the task
   */
  public RuntimeTaskColor getColor() { return this.color; }

  /**
   * Setter for the color of the task.
   * Might get removed in the future, as this method is very dangerous while being useless.
   * @param color the new color of the task
   */
  public void setColor(RuntimeTaskColor color) { this.color = color; }

  /**
   * Method used by the runtime to identify when the task gets active.
   * Should not be called by users.
   * @return the new number of awaited signals
   */
  public final int getDecrementedNBWait() { return --nbWait; }


  // 4. executing

  /**
   * The code of the task, called by a {@link RuntimeThread}.
   * Should not be called by users.
   * @param thread The thread running the task. Could be an interesting information for the task.
   */
  public abstract void run(RuntimeThread thread);




  ///////////////////////////////////////
  // Runtime mechanics



}
