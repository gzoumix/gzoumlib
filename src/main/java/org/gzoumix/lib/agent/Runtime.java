/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.agent;

import org.gzoumix.lib.agent.runtime.RuntimeSignal;
import org.gzoumix.lib.agent.runtime.RuntimeThread;
import org.gzoumix.lib.agent.runtime.RuntimeThreadMessage;

import java.util.ArrayList;


/**
 * This is the main class of the concurrent runtime provided as part of Gzoumlib.
 * The runtime takes inspiration from the <a href="https://github.com/etcimon/libasync">libasync</a> in which programs are structured in reactive colored tasks.
 * Tasks of the same color are executed in sequence, while different colors execute concurrently.
 * Tasks react to events.
 *
 * The implementation here is threaded, with tasks having complex guards and events having a <em>publish</em> semantics.
 * <ul>
 * <li><bf>Threaded</bf> means that the runtime can be initialized with several system thread (wrapped in {@link RuntimeThread}) which will execute the different tasks.</li>
 * <li><bf>Complex Guards</bf> means that a tasks may wait for more than one event before starting its execution</li>
 * <li><bf>Publish semantics</bf> means that when an event is sent, all the waiting tasks are notified of the event.</li>
 * </ul>
 *
 * An example of how to use this runtime is presented in the class <a href="https://bitbucket.org/gzoumix/gzoumlib/src/ebdf638bcc60f1937db825e648da411ec26bcad1/src/test/java/org/gzoumix/lib/TestRuntime.java?at=master&fileviewer=file-view-default">TestRuntime</a>
 */
public class Runtime {


  private ArrayList<RuntimeThread> threads;

  /**
   * Constructor of a Runtime Object
   */
  public Runtime() { this.threads = new ArrayList<>(); }

  /**
   * Adds a new thread to the runtime.
   * All the threads in a runtime must be different, and there should be at least one before calling the method {@link Runtime#start()}.
   * @param thread the new thread object added to the runtime
   */
  public void add(RuntimeThread thread) { this.threads.add(thread); }

  /**
   * Starts the runtime: all its thread start executing the different tasks they contain.
   */
  public void start() {
    for(RuntimeThread thread: this.threads) {
      //System.out.println("  Starting thread " + thread);
      thread.start();
    }
  }

  /**
   * Raises the signal in parameter: notifies the waiting task in all the threads
   * @param signal the raised signal
   */
  public void trigger(RuntimeSignal signal) {
    RuntimeThreadMessage message = RuntimeThreadMessage.SIGNAL(signal);
    for(RuntimeThread thread: this.threads) {
      thread.mail(message);
    }
  }

  /**
   * Gracefully stops the runtime: waits for all the running tasks to finish their execution. No new task will be executed after a call to {@link Runtime#stop()}
   */
  public void stop() {
    for(RuntimeThread thread: this.threads) {
      //System.out.println("  Stopping thread " + thread);
      thread.stop();
    }
  }

  /**
   * Returns the number of thread in this Runtime
   * @return the number of thread in this Runtime
   */
  public int getNBThread() { return this.threads.size(); }
}
