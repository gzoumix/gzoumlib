/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.agent.runtime;

import org.gzoumix.lib.agent.task.Task;
import org.gzoumix.lib.data.HashMapList;

import java.util.*;

/**
 * This class implements the colors in the runtime.
 * It collects all the tasks of this color and manage their wake-up
 */
public class RuntimeTaskColor {

  private HashMapList<RuntimeSignal,Task> waiting;   // the waiting tasks of the color
  private RuntimeThread thread;              // the thread managing that color
  private Queue<Task> active;                // tasks ready to execute
  private boolean idle;
  private boolean lock;                      // lock that may be used during work-stealing (not used for now)

  /**
   * Creates a new color managed by the {@link RuntimeThread} in parameter
   * @param thread the {@link RuntimeThread} managing this color
   */
  RuntimeTaskColor(RuntimeThread thread) {
    this.thread = thread;

    this.waiting = new HashMapList<>();
    this.active = new ArrayDeque<>();
    this.idle = true;
    this.lock = false;
  }

  /**
   * Adds a new task to the color
   * @param task the new task of this color
   * @param signals the set of signals the tasks must wait before starting its execution
   */
  public void add(Task task, Collection<RuntimeSignal> signals) {
    if(signals.isEmpty()) {
      this.active.offer(task);
    } else {
      for(RuntimeSignal signal: signals) { this.waiting.putEl(signal, task); }
    }
  }

  /**
   * returns the union of all the signals waited by this color's tasks
   * @return the union of all the signals waited by this color's tasks
   */
  public Set<RuntimeSignal> getSignals() { return this.waiting.keySet(); }

  /**
   * Informs the color that the signal in parameter have been triggered
   * @param signal the triggered signal
   * @return if one task of this color woke-up as a result of the trigger
   */
  public boolean wakeup(RuntimeSignal signal) {
    System.out.println("Signal " + signal + " received");
    boolean res = false;
    List<Task> tasks = this.waiting.remove(signal);
    if(tasks != null) {
      for(Task task: tasks) {
        if(task.getDecrementedNBWait() == 0) {
          this.active.offer(task);
          res = true;
          System.out.println("Task " + task + " woke up");
        }
      }
    }
    return res;
  }

  /**
   * returns if the color has a task ready for execution
   * @return if the color has a task ready for execution
   */
  public boolean hasNext() { return !this.active.isEmpty(); }

  /**
   * returns the next task of this color to execute
   * @return the next task of this color to execute
   */
  public Task next() { return this.active.poll(); }


  /**
   * returns if the color is locked. Not used for now, might be when work-stealing will be included in this implementation
   * @return if the color is locked
   */
  public boolean isLocked() { return lock; }

  /**
   * returns the {@link RuntimeThread} managing this color
   * @return the {@link RuntimeThread} managing this color
   */
  public RuntimeThread getThread() { return thread; }

  /**
   * Changes the {@link RuntimeThread} managing this color. Unsafe to use, as work-stealing is not really implemented
   * @param thread the new {@link RuntimeThread} managing this color
   */
  public void setThread(RuntimeThread thread) { this.thread = thread; }
}
