/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.agent.runtime;

import org.gzoumix.lib.agent.Runtime;
import org.gzoumix.lib.agent.task.Task;
import org.gzoumix.lib.data.HashMapSet;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * This class wraps a system thread, managing a set of colors and executing their active tasks in sequence.
 * This {@link RuntimeThread} also contains a mailbox for communication:
 *   currently, the only communication between thread are events, and so there is currently only one type of message.
 *   In the future, for work-stealing for instance, one can imagine other kind of messages.
 */
public class RuntimeThread implements Runnable {

  private Runtime runtime;              // the runtime of the current thread
  private Thread thread;                // the Java thread of the current thread
  private RuntimeThreadLock sleeping;   // used to make that thread sleep;
  private boolean keepup;               // global switch to stop the thread

  private Queue<RuntimeTaskColor> active;               // the active tasks
  private HashMapSet<RuntimeSignal, RuntimeTaskColor> waiting;

  private ConcurrentLinkedDeque<RuntimeThreadMessage> inbox; // the inbox of the thread

  /**
   * Creates a {@link RuntimeThread} in the given runtime
   * @param runtime the runtime managing this thread
   */
  public RuntimeThread(Runtime runtime) {
    this.runtime  = runtime;
    this.sleeping = new RuntimeThreadLock(false);
    this.keepup   = true;

    this.active  = new ArrayDeque<>();
    this.waiting = new HashMapSet<>();

    this.inbox   = new ConcurrentLinkedDeque<>();
  }


  ///////////////////////////////////////////////////////////////////////////////
  // getters

  /**
   * returns the runtime managing this thread
   * @return the runtime managing this thread
   */
  public Runtime getRuntime() { return this.runtime; }

  /**
   * returns the system thread wrapped by this thread
   * @return the system thread wrapped by this thread
   */
  public Thread getThread() { return this.thread; }

  /**
   * returns if the thread is executing a task or not
   * @return if the thread is executing a task or not
   */
  public boolean isSleeping() { return this.sleeping.value; }

  /**
   * returns if the runtime asked this thread to stop or not
   * @return if the runtime asked this thread to stop or not
   */
  public boolean isBeingStopped() { return !this.keepup; }

  /**
   * returns the set of active colors of this thread
   * @return the set of active colors of this thread
   */
  public Queue<RuntimeTaskColor> getActive() { return active; }

  /**
   * returns the inbox of this thread
   * @return the inbox of this thread
   */
  public ConcurrentLinkedDeque<RuntimeThreadMessage> getInbox() { return inbox; }

///////////////////////////////////////////////////////////////////////////////
  // setup and loop methods

  /**
   * Starts the execution of this thread: creates a system thread which executes the {@link RuntimeThread#run()} method
   */
  public void start() {
    this.thread = new Thread(this);
    this.thread.start();
  }

  /**
   * A thread runs a loop in three phases:
   * <ul>
   *   <li>first, it manages the received message, awaking the colors depending on the raised signals</li>
   *   <li>second, it looks if a color is active. If not, the thread goes to sleep waiting for a new message to arrive</li>
   *   <li>if there is an active color, it picks a task to execute and executes it</li>
   * </ul>
   */
  @Override
  public void run() {
    //System.out.println("Running the thread " + this + ", keepup=" + this.keepup + ", active=" + this.active.hasNext());
    while(this.keepup) {
      this.manageMailbox();

      while (!this.active.isEmpty()) {

        // 1. pick the color
        RuntimeTaskColor color = this.active.poll();

        if(color != null) {
          // 2. pick a task to run
          Task task = color.next();
          task.run(this);

          // 3. flush and replace the color
          this.putColor(color, color.hasNext());
        }

        if(!keepup) { return; }
        this.manageMailbox();
      }

      // 3. nothing to execute anymore: go to sleep
      synchronized (this.sleeping) {
        this.sleeping.value = true;
        while(this.sleeping.value && this.keepup) {
          //System.out.println("[" + this + "]: " + "going to sleep");
          try { this.sleeping.wait(); }
          catch (InterruptedException e) { e.printStackTrace(); }
        }
      }
    }
  }

  /**
   * Request this thread to gracefully stop its execution (finishing the execution of the running task if any)
   */
  public void stop() { this.keepup = false; }


  /**
   * Creates a new color managed by this thread
   * @param task the initial task for the new color
   * @param signals the guard for the initial task
   * @return the new color
   */
  public RuntimeTaskColor newColor(Task task, Collection<RuntimeSignal> signals) {
    RuntimeTaskColor res = new RuntimeTaskColor(this);
    this.putColor(res, task.publishWithColor(res, signals));
    return res;
  }


  ///////////////////////////////////////////////////////////////////////////////
  // management methods

  private void putColor(RuntimeTaskColor color, boolean active) {
    if(active) {
      this.active.offer(color);
    } else {
      for(RuntimeSignal signal: color.getSignals()) { this.waiting.putEl(signal, color); }
    }
  }


  private void manageMailbox() {
    while(!this.inbox.isEmpty()) {
      RuntimeThreadMessage message = this.inbox.poll();

      if(message.getKind() == RuntimeThreadMessage.Kind.SIGNAL) {
        RuntimeSignal signal = message.getSignal();
        Set<RuntimeTaskColor> colors = this.waiting.remove(signal);
        if(colors != null) {
          for(RuntimeTaskColor color: colors) {
            if(color.wakeup(signal)) {
              this.active.offer(color);
            }
          }
        }
      }
    }
  }

  /**
   * send a message to this thread's mailbox
   * @param message the message to send to this thread
   */
  public void mail(RuntimeThreadMessage message) {
    this.inbox.offer(message);
    if(this.sleeping.value) {
      synchronized (this.sleeping) {
        if(this.sleeping.value) {
          this.sleeping.value = false;
          //System.out.println("[" + this + "]: " + "going to wakeup");
          this.sleeping.notify();
        }
      }
    }
  }



  ///////////////////////////////////////////////////////////////////////////////
  // management class
  private static class RuntimeThreadLock {
    private boolean value;

    public RuntimeThreadLock(boolean value) { this.value = value; }

  }


}
