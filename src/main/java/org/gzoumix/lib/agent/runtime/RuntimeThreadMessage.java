/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.agent.runtime;

/**
 * This class implements the messages for inter-thread communication.
 * Currently, it only notifies threads about raised signals
 */
public class RuntimeThreadMessage {

  /**
   * This class is a type for the kind of message that can be sent to a thread
   */
  public static class Kind {

    /**
     * This kind is for messages that notify a signal being raised
     */
    public static final Kind SIGNAL = new Kind();

    private Kind() {}
  }


  /**
   * Factory for a message of kind {@link Kind#SIGNAL}
   * @param signal the raised signal
   * @return a corresponding message
   */
  public static RuntimeThreadMessage SIGNAL(RuntimeSignal signal) {
    return new RuntimeThreadMessage(Kind.SIGNAL, signal);
  }


  private Kind kind;
  private RuntimeSignal signal;


  private RuntimeThreadMessage(Kind kind, RuntimeSignal signal) {
    this.kind = kind;
    this.signal = signal;
  }

  /**
   * returns the kind of the message
   * @return the kind of the message
   */
  public Kind getKind() { return kind; }

  /**
   * returns the raised signal of this message
   * @return the raised signal of this message
   */
  public RuntimeSignal getSignal() { return signal; }
}
