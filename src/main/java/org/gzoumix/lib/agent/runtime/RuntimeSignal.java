package org.gzoumix.lib.agent.runtime;

import org.gzoumix.lib.agent.Runtime;

/**
 * This class gives an identity and a type to signals in the Gzoumlib runtime
 */
public class RuntimeSignal {
  private Runtime runtime;

  /**
   * Constructor for a signal
   * @param runtime the runtime in which this signal lives
   */
  public RuntimeSignal(Runtime runtime) { this.runtime = runtime; }

  /**
   * Alternative way to raise a signal: calling directly <em>trigger</em> on the signal instead of on the runtime
   */
  public void trigger() { this.runtime.trigger(this); }
}
