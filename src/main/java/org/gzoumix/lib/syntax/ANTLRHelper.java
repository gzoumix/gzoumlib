/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.syntax;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.gzoumix.lib.Global;
import org.gzoumix.lib.data.Log;
import org.gzoumix.lib.data.Pair;
import org.stringtemplate.v4.AutoIndentWriter;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.StringRenderer;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.*;


public class ANTLRHelper {


  /**
   * Get a position object for the region taken by the ParserRuleContext in parameter
   *
   * @param fileName the name of the file containing the region
   * @param ctx      the rule identifying the region
   * @return the position object delimiting the computed region
   */
  public static Position extractPositionFrom(String fileName, ParserRuleContext ctx) {
    int startLine = ctx.start.getLine();
    int startCharacterFile = ctx.start.getStartIndex();
    int startCharacterLine = ctx.start.getCharPositionInLine();
    int endLine = ctx.stop.getLine();
    int endCharacterFile = ctx.stop.getStopIndex();
    int endCharacterLine = ctx.stop.getCharPositionInLine();

    return new Position(fileName, startLine, startCharacterFile, startCharacterLine, endLine, endCharacterFile, endCharacterLine);
  }

  /**
   * Get a position object for the region taken by the TerminalNode in parameter
   *
   * @param fileName the name of the file containing the region
   * @param tn       the node identifying the region
   * @return the position object delimiting the computed region
   */
  public static Position extractPositionFrom(String fileName, TerminalNode tn) {
    int startLine = tn.getSymbol().getLine();
    int startCharacterFile = tn.getSymbol().getStartIndex();
    int startCharacterLine = tn.getSymbol().getCharPositionInLine();
    int endLine = tn.getSymbol().getLine();
    int endCharacterFile = startCharacterFile + tn.getSymbol().getText().length();
    int endCharacterLine = startCharacterLine + tn.getSymbol().getText().length();

    return new Position(fileName, startLine, startCharacterFile, startCharacterLine, endLine, endCharacterFile, endCharacterLine);
  }

  /**
   * This class gives a simple API for building an ANTLR parser, with error handling
   *
   * @param <P> The class of the parser to construct
   */
  public static class ParserConstructor<P extends Parser> implements ANTLRErrorListener {



    //////////////////////////////////////////////
    // 2. Class Definition

    private String fileName;
    private Class lexerClass;
    private Class<? extends P> parserClass;

    private Log log;

    /**
     * Constructor for this class
     *
     * @param lexer  the class for lexing the input stream
     * @param parser the class for the parser, which should be equal to or extend <code>P</code>
     */
    public ParserConstructor(Class<? extends Lexer> lexer, Class<? extends P> parser) {
      this.lexerClass = lexer;
      this.parserClass = parser;
    }

    public boolean hasError() {
      return (this.lexerClass == null) || (this.parserClass == null);
    }

    /**
     * Getter for the name of the file being parsed (or a dummy name when parsing a type or a stream)
     *
     * @return the name of the file (or a dummy name)
     */
    public String getFileName() {
      return fileName;
    }

    /**
     * Setter for the name of the file being parsed: useful when parsing a type of a stream for giving a non-dummy name to them
     *
     * @param fileName the fake file name
     */
    public void setFileName(String fileName) {
      this.fileName = fileName;
    }

    /**
     * Getter for the log object for this parser
     * @return the log object for this parser
     */
    public Log getLog() {
      return log;
    }

    /**
     * Setter for the lof object for this parser
     * @param log the new log object that will be put to log parsing errors
     */
    public void setLog(Log log) {
      this.log = log;
    }

    /**
     * function to parse a file
     *
     * @param fileName the file to parse
     * @return a parser of class <code>parserClass</code> parsing the file in parameter
     */
    public P file(String fileName) {
      this.setFileName(fileName);
      try {
        return this.parse(new ANTLRFileStream(fileName));
      } catch (IOException e) {
        if(this.log != null) {
          this.logError(new ParsingErrorException(e));
        }
        return null;
      }
    }

    /**
     * function to parse a stream
     *
     * @param stream the stream to parse
     * @return a parser of class <code>parserClass</code> parsing the stream in parameter
     */
    public P stream(InputStream stream) {
      this.setFileName("Stream");
      try {
        return this.parse(new ANTLRInputStream(stream));
      } catch (IOException e) {
        this.logError(new ParsingErrorException(e));
        return null;
      }
    }

    /**
     * function to parse a type
     *
     * @param code the type to parse
     * @return a parser of class <code>parserClass</code> parsing the type in parameter
     */
    public P string(String code) {
      this.setFileName("String");
      return this.parse(new ANTLRInputStream(code));
    }

    private P parse(ANTLRInputStream stream) {
      Lexer lexer = null;
      P parser = null;

      try { // create the lexer: NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException
        Constructor<Lexer> lexerConstructor = this.lexerClass.getConstructor(CharStream.class);
        lexer = lexerConstructor.newInstance(stream);
        lexer.removeErrorListeners();
        lexer.addErrorListener(this);
      } catch (Exception e) {
        this.logError(e);
      }

      if (lexer != null) {
        try { // create the parser: NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException
          CommonTokenStream tokens = new CommonTokenStream(lexer);
          Constructor<? extends P> parserConstructor = this.parserClass.getConstructor(TokenStream.class);
          parser = parserConstructor.newInstance(tokens);
          parser.removeErrorListeners();
          parser.addErrorListener(new DiagnosticErrorListener());
          parser.addErrorListener(this);
        } catch (Exception e) {
          this.logError(e);
        }
      }
      return parser;
    }


    private void logError(Object o) {
      if(this.log != null) {
        this.log.logError(o);
      }
    }



    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object o, int line, int c, String msg, RecognitionException e) {
      this.logError(new ParsingErrorSyntax(this.fileName, line, c, msg));
    }

    @Override
    public void reportAmbiguity(Parser parser, DFA dfa, int line, int c, boolean b, BitSet bitSet, ATNConfigSet atnConfigSet) {
    }


    //////////////////////////////////////////////
    // 3. Implementation of the ANTLRErrorListener interface

    @Override
    public void reportAttemptingFullContext(Parser parser, DFA dfa, int line, int c, BitSet bitSet, ATNConfigSet atnConfigSet) {
    }

    @Override
    public void reportContextSensitivity(Parser parser, DFA dfa, int line, int c1, int c2, ATNConfigSet atnConfigSet) {
    }

    private class ParsingErrorException {
      Exception e;

      ParsingErrorException(IOException e) {
        this.e = e;
      }

      @Override
      public String toString() {
        return e.getMessage();
      }
    }

    private class ParsingErrorSyntax {
      String fileName;
      int line, c;
      String msg;

      ParsingErrorSyntax(String fileName, int line, int c, String msg) {
        this.fileName = fileName;
        this.line = line;
        this.c = c;
        this.msg = msg;
      }

      @Override
      public String toString() {
        return "file \"" + this.fileName + "\": line " + this.line + ":" + this.c + " " + this.msg;
      }
    }
  }


  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////


}
