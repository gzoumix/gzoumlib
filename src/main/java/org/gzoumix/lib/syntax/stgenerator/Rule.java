/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.syntax.stgenerator;

import java.util.*;


/**
 * This class represents a antlr rule
 */
public class Rule {
  private STGenerator stg;
  private String name;
  private Kind kind;
  private List<Alternative> alternatives;
  private Set<ElementAtom> atoms;
  private List<ElementBlock> blocks;


  public static Rule lexerRule(STGenerator stg, String name) {
    Rule res = new Rule(stg, name, Kind.TERMINAL, null);
    return res;
  }

  public static Rule parserRule(STGenerator stg, String name) {
    Rule res = new Rule(stg, name, Kind.COMPOSITE, new LinkedList<>());
    return res;
  }

  public void addAlternative(Alternative alt) throws STGeneratorException {
    this.alternatives.add(alt);
    this.atoms.addAll(alt.getAtoms());
    this.blocks.addAll(alt.getBlocks());
  }

  public String getName() {
    return name;
  }
  public Kind getKind() {
    return kind;
  }
  public Collection<Alternative> getAlternatives() {
    return alternatives;
  }
  public Set<ElementAtom> getAtoms() { return atoms; }
  public List<ElementBlock> getBlocks() { return blocks; }

  public STGenerator getSTGenerator() {
    return stg;
  }

  public boolean isTerminal() {
    return this.kind == Kind.TERMINAL;
  }
  public boolean isComposite() {
    return this.kind == Kind.COMPOSITE;
  }


  public void checkRule() throws STGeneratorException {
    if(this.isComposite()) {
      for(Alternative alt: this.getAlternatives()) {
        alt.checkAlternative();
      }
    }
  }

  @Override
  public String toString() {
    return "Rule{" +
            "'" + name + "'" +
            ", kind=" + kind +
            ", alternatives=" + alternatives +
            '}';
  }

  private Rule(STGenerator stg, String name, Kind kind, List<Alternative> alts) {
    this.stg = stg;
    this.name = name;
    this.kind = kind;
    this.alternatives = alts;
    this.atoms = new HashSet<>();
    this.blocks = new LinkedList<>();
  }

  private enum Kind {TERMINAL, COMPOSITE}
}
