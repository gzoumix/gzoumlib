/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.syntax.stgenerator;

import org.antlr.parser.antlr4.ANTLRv4Parser;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public abstract class Element {
  public enum Arity {
    OPTIONAL, SINGLE, LIST, NON_EMPTY_LIST;

    public static Arity translate(ANTLRv4Parser.EbnfSuffixContext ctx) {
      if(ctx == null) {
        return SINGLE;
      } else if(ctx.PLUS() != null) {
        return (ctx.QUESTION(0) != null ? LIST : NON_EMPTY_LIST);
      } else if(ctx.STAR() != null) {
        return LIST;
      } else {
        return OPTIONAL;
      }
    }

    public Arity combine(Arity arity) {
      if(this == arity) {
        return this;
      } else if((this == LIST) || (arity == LIST)) {
        return LIST;
      }

      switch(this) {
        case OPTIONAL:
          switch(arity) {
            case SINGLE: return OPTIONAL;
            case NON_EMPTY_LIST: return LIST;
          }
        case SINGLE:
          switch(arity) {
            case OPTIONAL: return OPTIONAL;
            case NON_EMPTY_LIST: return NON_EMPTY_LIST;
          }
        case NON_EMPTY_LIST:
          switch(arity) {
            case OPTIONAL: return LIST;
            case SINGLE: return NON_EMPTY_LIST;
          }
      }
      return null;
    }

    public boolean isOptional() {
      return this == OPTIONAL;
    }

    public boolean isSingle() {
      return this == SINGLE;
    }

    public boolean isList() {
      return this == LIST;
    }

    public boolean isNonEmptyList() {
      return this == NON_EMPTY_LIST;
    }
  }

  protected String name;
  protected String parameterName;
  protected Set<ElementAtom> atoms;
  protected Arity arity;
  protected Alternative alt;


  public String getName() {
    return name;
  }
  public Set<ElementAtom> getAtoms() {
    return atoms;
  }
  public Arity getArity() {
    return arity;
  }
  public String getParameterName() {
    return parameterName;
  }
  public void setParameterName(String parameterName) {
    this.parameterName = parameterName;
  }
  public Alternative getAlt() { return alt; }

  abstract boolean isBlock();
  abstract List<ElementBlock> getBlocks();
}
