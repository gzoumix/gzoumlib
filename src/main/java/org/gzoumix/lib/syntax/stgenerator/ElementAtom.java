/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.syntax.stgenerator;

import java.util.*;

public class ElementAtom extends Element {
  private Kind kind;
  private String coreName;

  public ElementAtom(Alternative alt, String name, String coreName, Kind kind, Arity arity) {
    this.alt = alt;
    this.name = name;
    this.coreName = coreName;
    this.kind = kind;
    this.arity = arity;
    this.atoms = new HashSet<>();
    if(this.kind != Kind.STRING) {
      this.atoms.add(this);
    }
  }


  ////////////////////////////////////////////////////////////////////////
  // For StringTemplate

  public boolean isString() {
    return this.kind.equals(Kind.STRING);
  }
  public boolean isRule() {
    return this.kind.equals(Kind.RULE);
  }
  public boolean isTerminal() {
    return this.kind.equals(Kind.TERMINAL);
  }
  public boolean isNamed() { return !this.name.equals(this.coreName); }

  public String getCoreName() {
    return this.coreName;
  }
  public Kind getKind() {
    return this.kind;
  }

  public boolean isComposite() {
    if(this.isRule()) {
      return this.alt.getRule().getSTGenerator().getRule(this.getName()).isComposite();
    } else {
      return false;
    }
  }


  @Override
  boolean isBlock() {
    return false;
  }

  public String getName() {
    return name;
  }
  public Arity getArity() {
    return arity;
  }
  public String getParameterName() {
    return parameterName;
  }
  public void setParameterName(String parameterName) {
    this.parameterName = parameterName;
  }
  public Alternative getAlt() { return alt; }
  public List<ElementBlock> getBlocks() { return Collections.emptyList(); }


  @Override
  public String toString() {
    return "Atom{" +
            "kind=" + kind +
            ", arity=" + arity +
            ", coreName='" + coreName + '\'' +
            ", name='" + name + '\'' +
            '}';
  }

  public enum Kind { STRING, TERMINAL, RULE }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ElementAtom that = (ElementAtom) o;

    if (coreName != null ? !coreName.equals(that.coreName) : that.coreName != null) return false;
    return name.equals(that.name);

  }

  @Override
  public int hashCode() {
    int result = coreName != null ? coreName.hashCode() : 0;
    result = 31 * result + name.hashCode();
    return result;
  }
}
