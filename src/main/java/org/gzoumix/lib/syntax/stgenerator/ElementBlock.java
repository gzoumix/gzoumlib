/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.syntax.stgenerator;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class ElementBlock extends Element {
  private static int id_counter = 0;
  private int id;

  private List<Alternative> alternatives;
  private List<ElementBlock> blocks;

  public ElementBlock(Alternative alt, String name, Arity arity) {
    this.alt = alt;
    this.name = (name != null) ? name : ("Block" + (this.id = this.id_counter++));
    this.arity = arity;
    this.alternatives = new LinkedList<>();
    this.atoms = new HashSet<>();
    this.blocks = new LinkedList<>();
    this.blocks.add(this);
  }

  public void addAlternative(Alternative alt) {
    this.alternatives.add(alt);
    this.atoms.addAll(alt.getAtoms());
    this.blocks.addAll(alt.getBlocks());
  }


  public List<Alternative> getAlternatives() { return alternatives; }

  @Override
  public List<ElementBlock> getBlocks() {
    return blocks;
  }

  @Override
  public boolean isBlock() {
    return true;
  }

  public String getName() {
    return name;
  }
  public Arity getArity() {
    return arity;
  }
  public String getParameterName() {
    return parameterName;
  }
  public void setParameterName(String parameterName) { this.parameterName = parameterName; }
  public Alternative getAlt() { return alt; }


  ////////////////////////////////////////////////////////////////////////
  // For StringTemplate

  @Override
  public String toString() {
    return "ElementBlock{" +
            "'" + name + "'" +
            ", alternatives=" + alternatives +
            '}';
  }
}
