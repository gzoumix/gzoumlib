/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.syntax.stgenerator;

import org.antlr.parser.antlr4.ANTLRv4Parser;

public class Arity {

  private Element element;
  private int min;
  private int max;

  /////////////////////////////////////////////////////////////////////////////
  // CONSTRUCTORS
  public static Arity list() {
    return new Arity(0, -1);
  }

  public static Arity nonEmptyList() {
    return new Arity(1, -1);
  }

  public static Arity single() {
    return new Arity(1, 1);
  }

  public static Arity optional() {
    return new Arity(0, 1);
  }

  public static Arity translate(Element.Arity arity) {
    Arity res = null;
    switch(arity) {
      case OPTIONAL:
        res = Arity.optional();
        break;
      case SINGLE:
        res = Arity.single();
        break;
      case LIST:
        res = Arity.list();
        break;
      case NON_EMPTY_LIST:
        res = Arity.nonEmptyList();
        break;
    }
    return res;
  }

  public Arity(Arity arity) {
    this.element = arity.getElement();
    this.min = arity.getMin();
    this.max = arity.getMax();
  }

  private Arity(int min, int max) {
    this.min = min;
    this.max = max;
  }


  /////////////////////////////////////////////////////////////////////////////
  // GETTERS
  public int getMin() {
    return min;
  }

  public int getMinID() {
    return min - 1;
  }

  public int getMax() {
    return max;
  }

  public int getMaxID() {
    return max;
  }

  public boolean isList() {
    return (this.max == -1);
  }

  public boolean isOptional() {
    return (this.min == 0);
  }


  public void setElement(Element element) {
    this.element = element;
  }

  public void add(Arity arity) throws STGeneratorException {
    this.min = this.min + arity.min;
    if(this.isList()) {
      throw new STGeneratorException("Cannot add an element after a list");
    } else if(arity.isList()) {
      this.max = -1;
    } else {
      this.max = this.max + arity.max;
    }
  }

  public Element getElement() {
    return element;
  }

  public String getName() {
    return this.element.getName();
  }

  public boolean isComposite() {
    Rule rule = this.element.getAlt().getRule().getSTGenerator().getRule(this.getName());
    if(rule != null) {
      return rule.isComposite();
    } else {
      return false; // we suppose that things that are not declared are terminal
    }
  }


  public static Arity translate(ANTLRv4Parser.EbnfSuffixContext ctx) {
    if(ctx.PLUS() != null) {
      return new Arity((ctx.QUESTION(0) != null ? 0 : 1), -1);
    } else if(ctx.STAR() != null) {
      return Arity.list();
    } else {
      return Arity.optional();
    }
  }

  @Override
  public String toString() {
    return "{" +
            ((element != null) ? "element=" + element.getName() + ", ": "") +
            "min=" + min +
            ", max=" + max +
            '}';
  }
}
