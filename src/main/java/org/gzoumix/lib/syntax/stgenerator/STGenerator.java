/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.syntax.stgenerator;

import org.antlr.parser.antlr4.ANTLRv4Lexer;
import org.antlr.parser.antlr4.ANTLRv4Parser;
import org.gzoumix.lib.Global;
import org.gzoumix.lib.data.Log;
import org.gzoumix.lib.syntax.ANTLRHelper;
import org.stringtemplate.v4.AutoIndentWriter;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroupFile;
import org.stringtemplate.v4.StringRenderer;

import java.io.File;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.*;

public class STGenerator {
  private static final STGroupFile group;

  static {
    group = new STGroupFile("src/main/resources/STGenerator.stg");
    group.registerRenderer(String.class, new StringRenderer());
  }

  private Log log;

  // Configuration
  private String grammarFileName;
  private String stFileName;
  private PrintStream st;
  private String codeFileName;
  private PrintStream code;
  private String packageName;

  // Inner data
  private String grammarName;
  private Map<String, Rule> rules;


  public STGenerator(String grammarFileName) {
    this.grammarFileName = grammarFileName;

    this.rules = new HashMap<>();
  }

  public void setFileStringTemplate(String fileName) {
    this.stFileName = fileName;
  }

  public void setFileCode(String fileName) {
    this.codeFileName = fileName;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }


  public void generate() {
    try {
      File tmp;
      tmp = new File(this.stFileName);
      tmp.getParentFile().mkdirs();
      tmp.createNewFile();
      this.st = new PrintStream(tmp);

      tmp = new File(this.codeFileName);
      tmp.getParentFile().mkdirs();
      tmp.createNewFile();
      this.code = new PrintStream(tmp);
    } catch(Exception e) {
      e.printStackTrace();
      return;
    }

    // 1. Read the grammarFileName
    ANTLRv4Parser.GrammarSpecContext ctx = null;
    try {
      ANTLRHelper.ParserConstructor<ANTLRv4Parser> constructor = new ANTLRHelper.ParserConstructor<>(ANTLRv4Lexer.class, ANTLRv4Parser.class);
      ANTLRv4Parser parser = constructor.file(this.grammarFileName);
      ctx = parser.grammarSpec();
      Global.log.toStream(System.err);
    } catch (Exception e) {
      e.printStackTrace();
      return;
    }

    try {
      this.precheckGrammar(ctx);
      this.parseGrammar(ctx);
      this.check();
    } catch (STGeneratorException e) {
      e.printStackTrace();
      return;
    }

    // DEBUG
    //System.out.println(this.rules);

    // 3. create the files, calling the ST on ourselves
    ST stFileGenerator = this.group.getInstanceOf("stFile");
    stFileGenerator.add("STGeneratorObject", this);

    ST javaFileGenerator = this.group.getInstanceOf("javaFile");
    javaFileGenerator.add("STGeneratorObject", this);


    try {
      // 4.outputs the files
      Writer stWriter = new OutputStreamWriter(this.st);
      Writer codeWriter = new OutputStreamWriter(this.code);

      stFileGenerator.write(new AutoIndentWriter(stWriter));
      stWriter.flush();

      javaFileGenerator.write(new AutoIndentWriter(codeWriter));
      codeWriter.flush();

    } catch (Exception e) {
      e.printStackTrace(); // for now
    }
  }

  private void check() throws STGeneratorException {
    for(Rule rule: this.rules.values()) {
      rule.checkRule();
    }
  }


  private void parseGrammar(ANTLRv4Parser.GrammarSpecContext ctx) throws STGeneratorException {
    // 1. Get the Grammar Name
    this.grammarName = ctx.identifier().getText();

    // 2. Parse the rules
    ANTLRv4Parser.RulesContext rulesCtx = ctx.rules();
    for (ANTLRv4Parser.RuleSpecContext ruleSpecCtx : rulesCtx.ruleSpec()) {
      Rule rule = null;
      if (ruleSpecCtx.lexerRuleSpec() != null) {
        rule = this.parseLexerRule(ruleSpecCtx.lexerRuleSpec());
      } else {
        rule = this.parseParserRule(ruleSpecCtx.parserRuleSpec());
      }
      this.rules.put(rule.getName(), rule);
    }
  }


  private Rule parseLexerRule(ANTLRv4Parser.LexerRuleSpecContext ctx) {
    // we need to register this rule as a Lexer rule, so we access its token
    String name = ctx.TOKEN_REF().getSymbol().getText();
     return Rule.lexerRule(this, name);
  }


  private Rule parseParserRule(ANTLRv4Parser.ParserRuleSpecContext ctx) throws STGeneratorException {
    String ruleName = ctx.RULE_REF().getSymbol().getText();
    Rule rule = Rule.parserRule(this, ruleName);
    Alternative.resetID();

    for (ANTLRv4Parser.LabeledAltContext labeledAltCtx : ctx.ruleBlock().ruleAltList().labeledAlt()) {
      ANTLRv4Parser.AlternativeContext alternativeCtx = labeledAltCtx.alternative();
      Alternative alt = this.parseAlternative(rule, alternativeCtx, Element.Arity.SINGLE);
      rule.addAlternative(alt);
    }
    return rule;
  }


  private Alternative parseAlternative(Rule rule, ANTLRv4Parser.AlternativeContext alternativeCtx, Element.Arity arity) throws STGeneratorException {
    Alternative alt = new Alternative(rule);

    if (alternativeCtx.element() != null) {
      for (ANTLRv4Parser.ElementContext elementCtx : alternativeCtx.element()) {
        alt.addElement(this.parseElement(alt, elementCtx, arity));
      }
    }
    return alt;
  }


  private Element parseElement(Alternative alt, ANTLRv4Parser.ElementContext ctx, Element.Arity arity) throws STGeneratorException {
    // get the suffix of this element
    ANTLRv4Parser.EbnfSuffixContext ebnfSuffixCtx = ctx.ebnfSuffix();
    if ((ctx.ebnf() != null) && (ctx.ebnf().blockSuffix() != null)) {
      ebnfSuffixCtx = ctx.ebnf().blockSuffix().ebnfSuffix();
    }
    if (ebnfSuffixCtx != null) {
      arity = arity.combine(Element.Arity.translate(ebnfSuffixCtx));
    }

    // get the element definition
    String name = null;
    ANTLRv4Parser.AtomContext atomCtx = ctx.atom();
    if (ctx.labeledElement() != null) {
      name = ctx.labeledElement().identifier().getText();
      atomCtx = ctx.labeledElement().atom();
    }
    ANTLRv4Parser.BlockContext blockCtx = null;
    if (ctx.labeledElement() != null) {
      blockCtx = ctx.labeledElement().block();
    } else if (ctx.ebnf() != null) {
      blockCtx = ctx.ebnf().block();
    }

    // if atom
    if (atomCtx != null) {
      return this.parseElementAtom(alt, atomCtx, name, arity);
    }

    // if calling a block
    if (blockCtx != null) {
      return this.parseElementBlock(alt, blockCtx, name, arity);
    }

    throw new STGeneratorException("Only Atoms and Blocks have been implemented");
  }


  private Element parseElementAtom(Alternative alt, ANTLRv4Parser.AtomContext ctx, String name, Element.Arity arity) throws STGeneratorException {
    String type = ctx.getText();
    ElementAtom res = null;

    if (ctx.characterRange() != null) {
      if (name != null) {
        res = new ElementAtom(alt, name.substring(1, name.length() - 1), null, ElementAtom.Kind.STRING, arity); // how to get the type of this pattern?
      } else {
        throw new STGeneratorException("Unnamed Regular expressions in parser arities not managed for now");
      }
    } else if (ctx.terminal() != null) {
      if(ctx.terminal().STRING_LITERAL() != null) {
        type = type.substring(1, type.length() - 1);
        res = new ElementAtom(alt, name == null ? type : name, type, ElementAtom.Kind.STRING, arity);
      } else {
        res = new ElementAtom(alt, name == null ? type : name, type, ElementAtom.Kind.TERMINAL, arity);
      }
    }

    // if calling a rule
    if (ctx.ruleref() != null) {
      res = new ElementAtom(alt, name == null ? type : name, type, ElementAtom.Kind.RULE, arity);
    }

    return res;
  }

  public Element parseElementBlock(Alternative alt, ANTLRv4Parser.BlockContext ctx, String name, Element.Arity arity) throws STGeneratorException {
    ElementBlock res = new ElementBlock(alt, name, arity);

    List<ANTLRv4Parser.AlternativeContext> alternativetCtxList = ctx.altList().alternative();
    if (alternativetCtxList.size() > 1) {
      arity = arity.combine(Element.Arity.OPTIONAL);
    }
    for (ANTLRv4Parser.AlternativeContext alternativeCtx : alternativetCtxList) {
      Alternative innerAlt = this.parseAlternative(alt.getRule(), alternativeCtx, arity);
      res.addAlternative(innerAlt);
      //for (ANTLRv4Parser.ElementContext elementCtx : alternativeCtx.element()) {
      //  res.addSubElement(this.parseElement(elementCtx, new Arity(arity)));
      //}
    }
    return res;
  }


  private void precheckGrammar(ANTLRv4Parser.GrammarSpecContext ctx) throws STGeneratorException {
    if (!ctx.prequelConstruct().isEmpty()) {
      throw new STGeneratorException("Grammar with import: not supported for now");
    }
  }

  ////////////////////////////////////////////////////////////////////////
  // Getters
  public String getGrammarName() {
    return this.grammarName;
  }

  public Collection<Rule> getRules() {
    return this.rules.values();
  }

  public Rule getRule(String name) { return this.rules.get(name); }

  public String getSTFileName() { return this.stFileName; }


} // stg
