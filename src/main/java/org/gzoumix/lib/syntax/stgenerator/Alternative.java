/*
 *
 *  Copyright (C) 2015 Michael Lienhardt
 *
 *  This file is part of Gzoumlib
 *
 *  Gzoumlib is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Gzoumlib is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.gzoumix.lib.syntax.stgenerator;

import org.gzoumix.lib.Global;

import java.util.*;

public class Alternative {
  private static int id_counter = 0;
  private int id_counter_max = 0;
  private int id;

  public static void resetID() { id_counter = 0; }

  private Rule rule;

  private List<Element> elements;
  private Set<ElementAtom> atoms;
  private List<ElementBlock> blocks;

  private Map<String, Arity> arities;
  private Map<String, Element> parameters;

  public Alternative(Rule rule) {
    this.id = id_counter++;
    if(this.id_counter_max < this.id_counter) { this.id_counter_max = this.id_counter; }

    this.rule = rule;

    this.elements = new LinkedList<>();
    this.atoms = new HashSet<>();
    this.blocks = new LinkedList<>();
  }


  public void addElement(Element el) throws STGeneratorException {
    this.elements.add(el);
    this.atoms.addAll(el.getAtoms());
    this.blocks.addAll(el.getBlocks());
  }

  public void checkAlternative() throws STGeneratorException {
    this.parameters = new HashMap<>();
    this.arities = new HashMap<>();

    // look at all the elements of this alternative
    // compute the mapping "parameter name" -> atom
    // and compute the mapping "atom name" -> arity
    int id = 0;
    boolean add;
    String parameterName;
    // compute the parameters
    for (Element el : this.elements) {
      add = (el.isBlock()) || (!((ElementAtom) el).isString());
      if (add) {
        parameterName = this.getName() + el.getName() + (id++);
        this.parameters.put(parameterName, el);
        el.setParameterName(parameterName);

        // recursive call
        if(el.isBlock()) {
          for(Alternative alt: ((ElementBlock)el).getAlternatives()) {
            alt.checkAlternative();
          }
        }
      }
    }

    // compute the arities
    for(ElementAtom atom: this.getAtoms()) {
      if(!atom.isString()) {
        try {
          this.addArity(atom, atom.getName(), atom.getArity());
          if(atom.isNamed()) {
            this.addArity(atom, atom.getCoreName(), atom.getArity());
          }
        } catch(STGeneratorException e) {
          throw new STGeneratorException(this.getName() + ": element \"" + atom.getName() + "\"" + e.getMessage());
        }
      }
    }

    // if the number of parameter is empty, throw an exception
    if(this.arities.isEmpty()) {
      throw new STGeneratorException(this.getName() + ": Alternatives in parser rules must be identified");
    }
  }

  private void addArity(Element el, String name, Element.Arity arity) throws STGeneratorException {
    Arity ar = this.arities.get(name);
    if(ar != null) {
      ar.add(Arity.translate(arity));
    } else {
      ar = Arity.translate(arity);
      ar.setElement(el);
      this.arities.put(name, ar);
    }
  }


  public Set<String> getParameters() {
    return this.parameters.keySet();
  }

  public List<Element> getElements() {
    return elements;
  }
  public Set<ElementAtom> getAtoms() {
    return atoms;
  }
  public List<ElementBlock> getBlocks() {
    return blocks;
  }
  public Collection<Arity> getArities() { return arities.values(); }
  public Rule getRule() {
    return rule;
  }

  public String getName() {
    return Global.generateID(this.rule.getName() + "Alternative", this.id, this.id_counter_max);
  }


  @Override
  public String toString() {
    return "Alternative{" +
            "name='" + this.getName() + "'" +
            ", elements=" + elements +
            '}';
  }

} // Alternative
