/*
 * Copyright (C) 2015 Michael Lienhardt
 *
 * This file is part of Gzoumlib
 *
 * Gzoumlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gzoumlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with  Gzoumlib.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.gzoumix.lib.syntax;


/**
 * This class stores the position of a region of a file.
 */
public class Position {
  private String fileName;

  private int startLine;
  private int startCharacterFile, startCharacterLine;
  private int endLine;
  private int endCharacterFile, endCharacterLine;

  /**
   * The constructor of the class
   * @param file the file of the region
   * @param sl the starting line of the region
   * @param scf the starting character of the region from the start of the file
   * @param scl the starting character of the region from the start of the line
   * @param el the ending line of the region
   * @param ecf the ending character of the region from the start of the file
   * @param ecl the ending character of the region from the start of the line
   */
  public Position(String file, int sl, int scf, int scl, int el, int ecf, int ecl) {
    this.fileName = file;
    this.startLine = sl;
    this.startCharacterFile = scf;
    this.startCharacterLine = scl;
    this.endLine = el;
    this.endCharacterFile = ecf;
    this.endCharacterLine = ecl;
  }

  /**
   * @return the name of the file of the region
   */
  public String getFileName() { return fileName; }

  /**
   * @return the starting line of the region
   */
  public int getStartLine() { return startLine; }

  /**
   * @return the starting character of the region from the start of the file
   */
  public int getStartCharacterFile() { return startCharacterFile; }

  /**
   * @return the starting character of the region from the start of the line
   */
  public int getStartCharacterLine() { return startCharacterLine; }

  /**
   * @return the ending line of the region
   */
  public int getEndLine() { return endLine; }

  /**
   * @return the ending character of the region from the start of the file
   */
  public int getEndCharacterFile() { return endCharacterFile; }

  /**
   * @return the ending character of the region from the start of the line
   */
  public int getEndCharacterLine() { return endCharacterLine; }


  @Override
  public String toString() {
    return "file \"" + this.getFileName() + "\": line " + this.getStartLine() + ":" + this.getStartCharacterLine();
  }
}
