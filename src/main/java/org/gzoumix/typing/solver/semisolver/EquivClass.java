/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.semisolver;


import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gzoumix.typing.solver.semisolver.edge.EdgeSemi;
import org.gzoumix.typing.solver.semisolver.edge.EdgeUnif;
import org.gzoumix.typing.solver.semisolver.edge.IEdge;
import org.gzoumix.typing.solver.semisolver.history.EnumHistoryStep;
import org.gzoumix.typing.solver.semisolver.history.IHistory;
import org.gzoumix.typing.solver.term.ITerm;
import org.gzoumix.typing.solver.term.TermStructured;
import org.gzoumix.typing.solver.term.TermVariable;

class EquivClass<I extends IHistory> {

  private SemiSolver _solver;
  
  private ITerm _im;
  private Set<TermVariable> _dom;
  private Map<Integer,ITerm> _semiim;
  
  //private Dependency depends;

  private HistoryEquivClass _histories;
  
  /* 1. Constructors */
  
  public EquivClass(SemiSolver c, TermVariable v) {
    _solver = c;
    
    _im = v;
    _dom = new HashSet<TermVariable>();
    _dom.add(v);

    _semiim = new HashMap<Integer, ITerm>();
    
    //depends = null;
    //depends = new Dependency(_solver, this);

    //_histories = new HistoryEquivClass(this);
    _histories = new HistoryEquivClass();
  }

/*
  public EquivClass(Factory f, Constraint c, Variable v, TermStructured t) {
    factory = f;
    
    _im = t;
    _dom = new HashSet<Variable>();
    _dom.add(v);
    _isStructured = true;
    
    _semiim = new HashMap<Integer,Term>();
    
    depends = null;
    //solves = new HashSet<EquivClass>();
  }
*/


  /* 2. Getters */

  public ITerm getIm() { return _im; }
  public ITerm getSemi(Integer i) { return _semiim.get(i); }
  public boolean isStructured() { return (_im instanceof TermStructured); }
  public Set<TermVariable> dom() { return _dom; }
  public int size() { return _dom.size(); }


  //public void initDependencies() throws Dependency.LoopException { depends = new Dependency(semisolver, this); depends.init(); }
  //public Dependency getDepends() { return depends; }



  /* 4. Equivalence Class Extension */
  
  public List<IEdge<I>> addSemiIm(IEdge<I> history, TermVariable v, Integer i, ITerm t) /* throws Dependency.LoopException */ {
    List<IEdge<I>> res = new LinkedList<IEdge<I>>();
    //depends.addSemiIm(h, i, t);
    ITerm sim = _semiim.get(i);
    List<IHistory<I>> tmp = _histories.initHistoryList(history, v);
    if(sim == null) {
      _histories.put(i, history);
      _semiim.put(i, t);
      if(isStructured()) { tmp.add(_histories.getIm()); res.add(new EdgeSemi(tmp, EnumHistoryStep.STRUCTURAL_SEMI, _im, i, t)); }
    } else { tmp.add(_histories.getSemi(i)); res.add(new EdgeUnif(tmp, EnumHistoryStep.STRUCTURAL_SEMI, sim, t)); }
    
    return res;
  }

  public List<IEdge<I>> addSemiInf(IEdge<I> history, TermStructured t, Integer i, TermVariable v) /* throws Dependency.LoopException */ {
    List<IEdge<I>> res = new LinkedList<IEdge<I>>();
    //depends.addSemiInf(h, t, i);
    List<IHistory<I>> tmp = _histories.initHistoryList(history, v);

    if(_im instanceof TermStructured) {  tmp.add(_histories.getIm()); res.add(new EdgeSemi(tmp, EnumHistoryStep.STRUCTURAL_SEMI, t, i, _im)); }
    else {
      List<ITerm> subs = new LinkedList<ITerm>();
      for(ITerm st : t.getSubTerms()) { subs.add(_solver._factory.createTermVariableFromTerm(st)); }
      _im = _solver._factory.createTermStructured(t.getConstructor(), subs); // we need to say that this comes from the history h
      _histories.put(t, history);
      //depends.addUnif(hres1, _im); // update dependency and solve


      res.add(new EdgeSemi(tmp, EnumHistoryStep.GENERATIVE, t, i, _im));


      // add the semi-unification constraints.
      for(Map.Entry<Integer, ITerm> entry : _semiim.entrySet()) {
        tmp = new LinkedList<IHistory<I>>(); tmp.add(history); tmp.add(_histories.getSemi(entry.getKey()));
        res.add(new EdgeSemi(tmp, EnumHistoryStep.STRUCTURAL_SEMI, _im, entry.getKey(), entry.getValue()));
      }
    }
    return res;
  }

  
  public List<IEdge<I>> addUnif(IEdge<I> history, TermVariable v, ITerm t) /* throws Dependency.LoopException */ {
    List<IEdge<I>> res = new LinkedList<IEdge<I>>();
    //depends.addUnif(h, t);
    List<IHistory<I>> tmp = _histories.initHistoryList(history, v);

    if(t instanceof TermStructured) {
      if(_im instanceof TermStructured) {
        tmp.add(_histories.getIm());
        res.add(new EdgeUnif(tmp, EnumHistoryStep.STRUCTURAL_BASE, _im, t));
      } else {
        _histories.put((TermStructured)t, history); // we know why
        _im = t;
        for(Map.Entry<Integer, ITerm> entry : _semiim.entrySet()) {
          tmp = _histories.initHistoryList(history, v); tmp.add(_histories.getSemi(entry.getKey()));
          res.add(new EdgeSemi(tmp, EnumHistoryStep.STRUCTURAL_SEMI, t, entry.getKey(), entry.getValue()));
        }
      }
    } else {
      EquivClass eq = _solver.getEquivClass((TermVariable)t);
      if(this.size() < eq.size()) res = this.add(history, v, (TermVariable)t,  eq);
      else res = eq.add(history, (TermVariable)t, v, this);
    }
    return res;
  }
  
  
  /* 5. Merge of Equivalence Classes */

  public List<IEdge<I>> add(IEdge<I> history, TermVariable here, TermVariable there, EquivClass<I> eq) /* throws Dependency.LoopException */ {
    List<IEdge<I>> res = new LinkedList<IEdge<I>>();
    if(eq == this) return res;
    if(this._solver != eq._solver) return res; //throw new Exception("EquivClass.add(EquivClass): classes do not match");


    //depends.add(h, eq);
    _dom.addAll(eq._dom); // set the domain
    if (isStructured() && eq.isStructured()) { // set the image
      List<IHistory<I>> tmp = eq._histories.initHistoryList(history, there); // gives why "here" equals to the image of eq
      tmp.add(_histories.get(here)); // now we have everything
      res.add(new EdgeUnif(tmp, EnumHistoryStep.STRUCTURAL_BASE, this._im, eq._im));
    } else if (eq.isStructured()) {
      _im = eq._im;
    }
    // set the _semiim
    for(Map.Entry<Integer,ITerm> entry: eq._semiim.entrySet()) {
      ITerm t = _semiim.get(entry.getKey());
      if (t != null) {  // we have two semi-image for the same variable
        List<IHistory<I>> tmp = eq._histories.initHistoryList(history, there); // gives why "here" equals to the image of eq
        tmp.add(eq._histories.getSemi(entry.getKey()));
        tmp.add(_histories.getSemi(entry.getKey()));
        res.add(new EdgeUnif(tmp, EnumHistoryStep.STRUCTURAL_BASE, entry.getValue(), t));
      } else _semiim.put(entry.getKey(), entry.getValue());
    }

    // and finally, update the reference to eq
    Map<TermVariable, EquivClass> equiv = _solver.getEquivClassMapping();
    for(TermVariable v : eq.dom()) equiv.put(v, this);
    //for(EquivClass tmp : _solver.getEquivClass()) tmp.depends.replace(eq, this);

    // extend history
    _histories.putAll(eq._histories);

    return res;
  }




/*

  // TODO: the current implementation of this method is bad.
  // I need to change everything by actually merging equivalence classes as done in the previous method.

  public List<IEdge> putIn(SemiSolver c, Map<EquivClass, EquivClass> mapRes) throws Dependency.LoopException {
    List<IEdge> res = new LinkedList<IEdge>();
    if(c == this._solver) return res;
    
    // 1. Compute the set of EquivClass with which we fuse
    Set<EquivClass> seq = c.getPartialEquivClass(this._dom);

    // 2. find one EquivClass where to put [this]
    EquivClass newthis;
    if(seq.hasNext()) { newthis = c.getEquivClass(this._dom.iterator().next()); }
    else { newthis = seq.iterator().next(); }

    // 3. Update [seq]
    seq.remove(newthis);
    seq.add(this);

    // 4. Add all equiv class in [seq] to [newthis]
    for(EquivClass eq : seq) {
      // 4.1. _dom
      newthis._dom.addAll(eq._dom);
      // 4.2. _histories
      newthis._histories.putAll(eq._histories);
      // 4.3. _im and (_semiim vs _im)
      if((newthis._im instanceof TermStructured)  && (eq._im instanceof TermStructured)) {
        List<IHistory<I>> tmp = new LinkedList<IHistory<I>>();
        tmp.add(eq._histories.getIm());
        tmp.add(newthis._histories.getIm());
        res.add(new EdgeUnif(tmp, EnumHistoryStep.STRUCTURAL_BASE, newthis._im, eq._im));
      } else if(eq._im instanceof TermStructured) {
        newthis._im = eq._im;
        newthis._histories.put();
        newthis._isStructured = true;
        History heq = eq._histories.getIm();
        for(Map.Entry<Integer, ITerm> entry : newthis._semiim.entrySet()) {
          History hnt = newthis._histories.getSemiIm(entry.getKey());
          History hres = HistoryFactory.putInCoStepTwo(hnt, newthis._histories.findHistory(hnt.getVariable(), heq.getVariable()), heq); // ([t] = [a]) ~ ([b] <_i [t'])
          res.add(new EdgeSemi(hres, eq.getIm(), entry.getKey(), entry.getValue()));
        }
      } else if(newthis._isStructured) {
        History hnt = newthis._histories.getIm();
        for(Map.Entry<Integer, ITerm> entry : eq._semiim.entrySet()) {
          History heq = eq._histories.getSemiIm(entry.getKey());
          History hres = HistoryFactory.putInCoStepThree(hnt, newthis._histories.findHistory(hnt.getVariable(), heq.getVariable()), heq); // ([t] = [a]) ~ ([b] <_i [t'])
          res.add(new EdgeSemi(hres, newthis.getIm(), entry.getKey(), entry.getValue()));
        }
      }
      // 4.4. _semiim
      for(Map.Entry<Integer, ITerm> entry : eq._semiim.entrySet()) {
        if(newthis._semiim.containsKey(entry.getKey())) {
          History hnt = newthis._histories.getSemiIm(entry.getKey());
          History heq = eq._histories.getSemiIm(entry.getKey());
          History hres = HistoryFactory.putInCoStepFour(hnt, newthis._histories.findHistory(hnt.getVariable(), heq.getVariable()), heq); // ([a] <_i [t]) ~ ([b] <_i [t'])
          res.add(new EdgeUnif(hres, newthis._semiim.get(entry.getKey()), entry.getValue()));
        } else { newthis._semiim.put(entry.getKey(), entry.getValue()); }
      }
      // 4.5. depends
      newthis.depends.add(null, eq);
      // 4.6. update references in [c]
      Map<TermVariable, EquivClass> map = c.getEquivClassMapping();
      for(TermVariable v : eq._dom) { map.put(v, newthis); }
      // 4.7. finally set the map in parameter
      mapRes.put(this, newthis);
    }

    return res;
  }

*/

/*
    // 1. Create a "copy" in c
    TermVariable idv = factory.freshTermVariableFromTerm(this._im);
    EquivClass ideq = c.getEquivClass(idv);
    ideq._im = this._im;
    ideq._isStructured = this._isStructured;
    ideq._semiim.putAll(this._semiim);
    ideq.depends = this.depends.copy();
    mapRes.put(this, ideq);
    
    // 1.2. generate the history for ideq
    ideq._histories.putAll(_histories); // we put why the variables in ideq have the current _im and _semiim
    History hres = HistoryFactory.putInCoStepOne(); // create an empty history for idv = v
    Iterator<Variable> ith = _dom.iterator(); Variable testy = ith.next();
    new UnifEdge(hres, idv, testy); // set the edge in the history
    ideq._histories.put(idv, hres); // and put the equivalence between the two variable in the class
    ideq._histories.put(testy, hres);


    // 2. link this copy to the other equivalence class in c
    Set<Variable> svc = c._dom();
    Set<Variable> tmp = new HashSet<Variable>(); tmp.addAll(this._dom);
    Iterator<Variable> it = tmp.iterator();
    while(it.hasNext()) {
      Variable v = it.next();
      if(_dom.contains(v)) {
        History hresNext = HistoryFactory.putInCoStepTwo(ideq._histories.findHistory()); // <- cosa metterci ??
        res.add(new UnifEdge(idv, factory.termVariable(v))); // <- viene dal fatto che abbiamo già il vincolo. Si deve quindi trovare, 
        tmp.removeAll(c.getEquivClass(v)._dom);
      } else {
        ideq._dom.add(v);
      }
    }
    return res;
  }*/



  public String toString(Map<EquivClass, Integer> m) {
    String res = "      Image = " + (isStructured() ? "[s]" : "[v]") + (_im.toString()) + "\n";
    res       += "      Domain = "; for(TermVariable v : _dom) { res += v.toString() + " "; } res += "\n";
    res       += "      Semi Image = "; if(_semiim.isEmpty()) { res += "Empty\n"; } else {
      for(Map.Entry<Integer, ITerm> entry : _semiim.entrySet()) { res += "(" + (entry.getKey()) + " = " + (entry.getValue().toString()) + ")";
      } res += "\n"; }
    //res       += "      Depends on = \n" + depends.toString(m);
    return res;
  }
}



