package org.gzoumix.typing.solver.semisolver.history;

public enum EnumHistoryStep {
  BASE, STRUCTURAL_BASE, STRUCTURAL_SEMI, GENERATIVE, UNFICATION_BASE, UNIFICATION_SEMI;
}
