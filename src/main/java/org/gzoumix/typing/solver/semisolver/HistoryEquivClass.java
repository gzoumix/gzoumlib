/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.semisolver;

import java.util.*;

import org.gzoumix.typing.solver.semisolver.edge.IEdge;
import org.gzoumix.typing.solver.semisolver.history.IHistory;
import org.gzoumix.typing.solver.term.TermStructured;
import org.gzoumix.typing.solver.term.TermVariable;

/** this class store the history of generation of that equivalence class */
class HistoryEquivClass<I extends IHistory> {

  /** this is the history for the construction of the image of the class */
  private IEdge<I> _im;                     // the edge that set the current image of the equivalence class
  private Map<TermVariable, IEdge<I>> _dom; // the edge that added the variable to the equivalence class
  private Map<Integer, IEdge<I>> _semiim;   // the edge that added the semi term to the equivalence class
  //private EquivClass _owner;

  /* 1. Constructor */
  //public HistoryEquivClass(EquivClass eq) { _im = null; _dom = new HashMap<TermVariable, Set<IEdge<I>>>(); _semiim = new HashMap<Integer, IEdge<I>>(); _owner = eq; }
  public HistoryEquivClass() { _im = null; _dom = new HashMap<TermVariable, IEdge<I>>(); _semiim = new HashMap<Integer, IEdge<I>>(); }


  /* 3. Getters */
  public IEdge<I> getIm() { return _im; }
  public IEdge<I> getSemi(Integer i) { return _semiim.get(i); }
  public IEdge<I> get(TermVariable v) { return _dom.get(v); }


  List<IEdge<I>> initHistoryList(IEdge<I> history, TermVariable v) {
    List<IEdge<I>> res = new LinkedList<IEdge<I>>();

    if(history != null) res.add(history);

    if(v != null) {
      IEdge<I> e = get(v);
      if(e != null) { res.add(e); }
    }
    return res;
  }


  /* 2. Basic Extension */
  public void put(TermStructured t, IEdge<I> h) { if(getIm() == null) { _im = h; } }
  public void put(TermVariable v, IEdge<I> h) { _dom.put(v, h); }
  public void put(Integer i, IEdge<I> h) { if(getSemi(i) == null) { _semiim.put(i, h); } }

  public void putAll(HistoryEquivClass histories) {
    _dom.putAll(histories._dom);
    _semiim.putAll(histories._semiim);
  }

  /*
  public void putAll(HistoryEquivClass heq) {
    if(_im == null) { _im = heq._im; }
    for(Map.Entry<TermVariable, Set<History>> entry : heq._dom.entrySet()) {
      Set<History> res = this._dom.get(entry.getKey());
      if(res == null) { res = new HashSet<History>(); this._dom.put(entry.getKey(), res); }
      res.addAll(entry.getValue());
    }
    for(Map.Entry<Integer, History> entry : heq._semiim.entrySet()) {
      if(!this._semiim.containsKey(entry.getKey())) { this._semiim.put(entry.getKey(), entry.getValue()); }
    }
  }*/



  /* 4. Path Finding *//*
  HistoryPath findHistory(TermVariable v1, TermVariable v2) { return findHistory(v1, HistoryType.unif(v2)); }
  HistoryPath findHistory(TermVariable v) { return findHistory(v, HistoryType.unif(_owner.getIm())); }
  HistoryPath findHistory(TermVariable v, Integer i) { return findHistory(v, HistoryType.semi(i, _owner.getSemiIm(i))); }


  HistoryPath findHistory(TermVariable var, HistoryType t) { // follow the graph of variables from [v], to find the term in t.
    Set<TermVariable> checked = new HashSet<TermVariable>();
    Set<TermVariable> toCheck = new HashSet<TermVariable>();
    toCheck.add(var);

    Map<TermVariable, HistoryPath> map = new HashMap<TermVariable, HistoryPath>();

    try{
    while(!toCheck.hasNext()) {
      Set<TermVariable> tmp = toCheck;
      toCheck = new HashSet<TermVariable>();
      for(TermVariable v : tmp) {
        HistoryPath path = map.get(v);
        for(History h : _dom.get(v)) {
          if(t.validates(h)) { return new HistoryPath(path, h); }
          TermVariable follow = h.getFollowing(v);
          if((follow != null) && (!checked.contains(follow))) {toCheck.add(follow); map.put(follow, new HistoryPath(path, h)); }
        }
        checked.add(v);
      }
    }
    } catch(Exception e) { e.printStackTrace(); }
    System.out.println("Error while looking for a path for \"" + var.toString() + t.toString() + "\"");
    System.out.println("Available edges:");
    for(Map.Entry<TermVariable, Set<History>> entry : _dom.entrySet()) {
      System.out.print("  " + entry.getKey().toString() + " => ");
      Iterator<History> ite = entry.getValue().iterator();
      while(ite.hasNext()) {
        System.out.print(ite.next().getEdge().toString());
        if(ite.hasNext()) { System.out.print(" /\\ "); }
      } System.out.println("");

    }
    System.out.println(_owner.toString(new HashMap<EquivClass, Integer>()));

    return null;
  }*/



}


