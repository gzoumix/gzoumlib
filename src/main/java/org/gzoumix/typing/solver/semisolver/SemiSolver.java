/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.semisolver;



import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.io.PrintStream;

import org.gzoumix.typing.solver.semisolver.edge.EdgeSemi;
import org.gzoumix.typing.solver.semisolver.edge.EdgeUnif;
import org.gzoumix.typing.solver.semisolver.edge.IEdge;
import org.gzoumix.typing.solver.semisolver.error.IError;
import org.gzoumix.typing.solver.semisolver.error.ErrorUnif;
import org.gzoumix.typing.solver.semisolver.history.EnumHistoryStep;
import org.gzoumix.typing.solver.semisolver.history.IHistory;
import org.gzoumix.typing.solver.term.Factory;
import org.gzoumix.typing.solver.term.ITerm;
import org.gzoumix.typing.solver.term.TermStructured;
import org.gzoumix.typing.solver.term.TermVariable;
import org.gzoumix.typing.solver.function.Substitution;

public class SemiSolver<I extends IHistory> {

  private static int _C_semiCounter = 0;

  Factory _factory;      // the factory used to create new terms for the control of semi-unification
  private boolean _hasRecursion; // boolean checking if we found a loop somewhere. For now this is a hard error which stops the solving process
  
  private Set<IEdge<I>> _edges;                    // the edges we already managed (we compute a fix point)
  private Map<TermVariable,EquivClass<I>> _equivs; // the equivalence class of variables
  
  private List<IEdge> _queue;                   // the edges we still need to manage
  private List<IError> _errors;

  private PrintStream debugFile;
  
  public SemiSolver(Factory factory) {
    _factory = factory;
    _hasRecursion = false;
    
    _edges = new HashSet<IEdge<I>>();
    _equivs = new HashMap<TermVariable,EquivClass<I>>();
    
    _queue = new LinkedList<IEdge>();
    
    _errors = new LinkedList<IError>();

    debugFile = null;
  }
  
  /*********************************/
  /* First Part: Utility Functions */
  /*********************************/

  
  public List<IError> getErrors() { return _errors; }
  public boolean hasRecursion() { return _hasRecursion; }
  
  public Substitution getSubstitution() {
    SubstitutionGraph g = new SubstitutionGraph();
    g.<I>init(this);
    return g.getSubstitution(_factory);
  }

  public Set<TermVariable> dom() {
    Set<TermVariable> res = new HashSet<TermVariable>();
    for(Map.Entry<TermVariable, EquivClass<I>> entry : _equivs.entrySet()) { res.add(entry.getKey()); }
    return res;
  }

  public Set<EquivClass<I>> getEquivClass() {
    Set<EquivClass<I>> res = new HashSet<EquivClass<I>>();
    for(Map.Entry<TermVariable, EquivClass<I>> entry : _equivs.entrySet()) {
      res.add(entry.getValue()); }
    return res;
  }

  public EquivClass<I> getEquivClass(TermVariable v) {
    EquivClass<I> eq = _equivs.get(v);
    if(eq == null) { eq = new EquivClass(this, v); _equivs.put(v, eq); }
    return eq;
  }

  public Set<EquivClass> getEquivClass(Set<TermVariable> vs) {
    Set<EquivClass> res = new HashSet<EquivClass>();
    for(TermVariable v : vs) { res.add(this.getEquivClass(v)); }
    return res;
  }

  public Set<EquivClass> getPartialEquivClass(Set<TermVariable> svar) {
    Set<EquivClass> res = new HashSet<EquivClass>();
    EquivClass tmp; for(TermVariable v : svar) { tmp = _equivs.get(v); if(tmp != null) { res.add(tmp); } }
    return res;
  }


  Map<TermVariable, EquivClass<I>> getEquivClassMapping() { return _equivs; }


  private void debug (String s) { if(debugFile != null) {debugFile.println(s); } }
  
  
  /******************************/
  /* Second Part: Edge addition */
  /******************************/

  
  private void addEdge(IEdge<I> e) {
    debug("  Adding edge: " + e.toString());
    if (_edges.contains(e)) { }
    else { _edges.add(e); _queue.add(e); }
  }



  
  public void addEquation(IHistory<I> history, ITerm t1, ITerm t2) {
    if (t1 == t2) return;
    
    if ((t1 != null) && (!t1.hasNull())) {
      if ((t2 != null) && (!t2.hasNull())) {
        List<IHistory<I>> tmp = new LinkedList<IHistory<I>>(); tmp.add(history);
        addEdge(new EdgeUnif(tmp, EnumHistoryStep.BASE, t1, t2));
      } else throw new NullPointerException("Constraint.addEquation: The second term contains a forbidden null pointer");
    } else throw new NullPointerException("Constraint.addEquation: The first term contains a forbidden null pointer");
  }
  
  public void addSemiEquation(IHistory<I> history, ITerm t1, ITerm t2) {
    if (t1 == t2) return;

    if ((t1 != null) && (!t1.hasNull())) {
      if ((t2 != null) && (!t2.hasNull())) {
        Integer i = new Integer(_C_semiCounter);
        _C_semiCounter += 1;
        List<IHistory<I>> tmp = new LinkedList<IHistory<I>>(); tmp.add(history);
        addEdge(new EdgeSemi(tmp, EnumHistoryStep.BASE, t1, i, t2));
      } else throw new NullPointerException("Constraint.addSemiEquation: The second term contains a forbidden null pointer");
    } else throw new NullPointerException("Constraint.addSemiEquation: The first term contains a forbidden null pointer");
  }

  void addAllEquation(List<IEdge<I>> l) { for(IEdge e: l) { addEdge(e); } }


  // Possible bugs: for the merge of equivalence classes, the edge is null, and the two variables are equals
  public void add(SemiSolver<I> c) /* throws Dependency.LoopException */ {
    
    // For simple structure, simple union
    _edges.addAll(c._edges);
    _queue.addAll(c._queue);
    _errors.addAll(c._errors);

    // Equivalence Class
    // principle:
    Set<EquivClass<I>> eqs = c.getEquivClass();
    eqs.addAll(this.getEquivClass());

    Map<TermVariable, EquivClass<I>> map = new HashMap<>();

    while(!eqs.isEmpty()) {
      TermVariable base_key = eqs.iterator().next().dom().iterator().next(); // pick a variable from the set
      Set<EquivClass<I>> value = new HashSet<>();

      // computation of the transitive closure of [key]
      Set<TermVariable> to_add = new HashSet<>(); to_add.add(base_key);
      while(!to_add.isEmpty()) {
        Set<TermVariable> tmp = new HashSet<>();
        for(TermVariable v : to_add) { for(EquivClass<I> eq : eqs) {
          if(eq.dom().contains(v)) {
            tmp.addAll(eq.dom());
            value.add(eq);
          }
        } }
        tmp.removeAll(to_add);
        to_add = tmp;
      }
      // now we have the right set of equivalence classes
      eqs.removeAll(value);

      // do the merge of the equivalence classes
      EquivClass<I> eq = value.iterator().next(); // pick an equivalence class: exists
      value.remove(eq);
      for(EquivClass<I> eq_other : value) { eq.add(null, base_key, base_key, eq_other); }
      for(TermVariable v: eq.dom()) { map.put(v, eq); }
    }
    // update the equivalence classes
    this._equivs = map;

    //try {
    //  1. add the classes in the semisolver
    //  Map<EquivClass, EquivClass> map = new HashMap<EquivClass, EquivClass>();
    //  for(EquivClass eq: c.getEquivClass()) { this.addAllEquation(eq.putIn(this, map)); }
    //  2. Update the references in the dependencies: no dependency in current version
    //  for(EquivClass ideq: map.values()) {
    //    for(Map.Entry<EquivClass, EquivClass> entry : map.entrySet()) {
    //      ideq.getDepends().replace(entry.getKey(), entry.getValue());
    //    }
    //  }
    //} catch(Dependency.LoopException exp) {
    //  System.err.println("Dependency.LoopException caught in \"add(Constraint)\": couldn't occur here!!??"); }
  }
  
  /*****************************/
  /* Third Part: Closure Rules */
  /*****************************/

  private void ruleOne(IHistory<I> history, TermStructured t1, TermStructured t2) {
    if ((t1.getConstructor().equals(t2.getConstructor())) && (t1.getSubTerms().size() == t2.getSubTerms().size())) {
      List<IHistory<I>> tmp = new LinkedList<IHistory<I>>(); tmp.add(history);
      Iterator<ITerm> i1 = t1.getSubTerms().iterator();
      Iterator<ITerm> i2 = t2.getSubTerms().iterator();
      while(i1.hasNext()) { addEdge(new EdgeUnif(tmp, EnumHistoryStep.STRUCTURAL_BASE, i1.next(), i2.next())); }
    } else {
      _errors.add(new ErrorUnif(history, t1, t2));
      debug("  Error: Impossible to unify!");
    }
  }


  private void ruleTwo(IHistory<I> history, TermStructured t1, Integer i, TermStructured t2) {
    if ((t1.getConstructor().equals(t2.getConstructor())) && (t1.getSubTerms().size() == t2.getSubTerms().size())) {
      List<IHistory<I>> tmp = new LinkedList<IHistory<I>>(); tmp.add(history);
      Iterator<ITerm> i1 = t1.getSubTerms().iterator();
      Iterator<ITerm> i2 = t2.getSubTerms().iterator();
      while(i1.hasNext()) { addEdge(new EdgeSemi(tmp, EnumHistoryStep.STRUCTURAL_BASE, i1.next(), i, i2.next())); }
    } else {
      _errors.add(new ErrorUnif(history, t1, i, t2));
      debug("  Error: Impossible to unify!");
    }
  }
    
  /**********************************/
  /* Fourth Part: Solving Algorithm */
  /**********************************/

  private void solveEdge(IEdge<I> e) {
    ITerm t1 = e.getLeft();
    ITerm t2 = e.getRight();

   debug("Solve: Dealing with the edge: " + e.toString() + " ...     ");
    
    //try {
      if(e instanceof EdgeUnif) {
        if(t1 instanceof TermVariable) { addAllEquation(getEquivClass((TermVariable)t1).addUnif(e, (TermVariable)t1, t2)); }
        else if(t2 instanceof TermVariable) { addAllEquation(getEquivClass((TermVariable)t2).addUnif(e, (TermVariable)t2, t1)); }
        else { ruleOne(e, (TermStructured)t1, (TermStructured)t2); }
      } else {
        Integer i = ((EdgeSemi)e).getI();
        if(t1 instanceof TermVariable) { addAllEquation(getEquivClass((TermVariable)t1).addSemiIm(e, (TermVariable)t1, i, t2)); }
        else if(t2 instanceof TermVariable) { addAllEquation(getEquivClass((TermVariable)t2).addSemiInf(e, (TermStructured)t1, i, (TermVariable)t2)); }
        else { ruleTwo(e, (TermStructured)t1, i, (TermStructured)t2); }
      }
    //} catch(Dependency.LoopException exp) {
    //  debug("  Error: Recursive definition found!");
    //  _hasRecursion = true;
    //  _errors.add(new ErrorLoop(e, exp.getLoop()));
    //}
  }
  
  public void solve() {
    while(!_queue.isEmpty()) {
      List<IEdge> tmp = _queue;
      _queue = new LinkedList<IEdge>();
      for(IEdge e : tmp) {
        solveEdge(e);
      }
    }
  }

  /************************/
  /* Sixth Part: ToString */
  /************************/

  
  public String toString() {
    
    String res =  "Constraint: " + (_errors.isEmpty()? "[true]\n" : "[false]\n");
    // 1. print the _queue
    res = res + "  Queue = " ;
    Iterator<IEdge> ite = _queue.iterator();
    while(ite.hasNext()) {
      res += ite.next().toString();
      if(ite.hasNext()) res = res + " /\\ ";
    } res = res + "\n";
    /*// 2. print the unification Graph
    res = res + "  Equation Graph = ";
    Iterator<UnifEdge> itu = unifEdges.iterator();
    while(itu.hasNext()) {
      res += itu.next().toString();
      if(itu.hasNext()) res = res + " /\\ ";
    }
    // 3. print the semi-unification Graph
    res = res + "  Semi-Equation Graph = \n     ";
    Iterator<SemiEdge> its = semiEdges.iterator();
    while(its.hasNext()) {
      res += its.next().toString();
      if(its.hasNext()) res = res + " /\\ ";
    }*/
    // 4. finally, the equivalence class
    int id = 0;
    Map<EquivClass, Integer> m = new HashMap<EquivClass, Integer>();
    Set<EquivClass<I>> tmp = this.getEquivClass();
    for(EquivClass eq : tmp) { 
        m.put(eq, new Integer(id)); id += 1; 
    }
 
    res = res + "  Equivalence Classes = "; 
    if(tmp.isEmpty()) { 
        res += "Empty"; 
    } else {
        for(EquivClass eq : tmp) {
        res += "\n    Class " + m.get(eq).toString() + "\n" + eq.toString(m);
        }
    } 
    
    res += "\n";    
    return res;
  }
  
}


