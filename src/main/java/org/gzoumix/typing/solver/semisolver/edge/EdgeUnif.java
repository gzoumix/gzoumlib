/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  This file is part of libgzoumix                                       */
/*  Copyright (C) 2014. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 3 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.semisolver.edge;

import org.gzoumix.typing.solver.semisolver.history.EnumHistoryStep;
import org.gzoumix.typing.solver.semisolver.history.IHistory;
import org.gzoumix.typing.solver.term.ITerm;

import java.util.LinkedList;
import java.util.List;

public class EdgeUnif<I extends IHistory> implements IEdge<I> {

  private ITerm t1, t2;

  private List<IHistory<I>> _history;
  private EnumHistoryStep _origin;

  public EdgeUnif(List<IHistory<I>> origin, EnumHistoryStep step, ITerm t1, ITerm t2) {
    _history = origin; _origin = step;
    this.t1 = t1; this.t2 = t2;
  }

  /* IEdge */
  public ITerm getLeft() { return t1; }
  public ITerm getRight() { return t2; }

  /* IHistory */
  @Override
  public List<I> getBaseHistory() {
    List<I> res = new LinkedList<I>();
    for(IHistory<I> e: _history) { res.addAll(e.getBaseHistory()); }
    return res;
  }

  @Override
  public EnumHistoryStep getOrigin() { return _origin; }


  /* Comparable */
  @Override
  public int compareTo(Object o) {
    if(o instanceof EdgeUnif) {
      int tmp = this.t1.compareTo(((EdgeUnif)o).t1);
      if(tmp != 0) { return tmp; }
      else { return this.t2.compareTo(((EdgeUnif)o).t2); }
    }
    else return -1;
  }

  /* Object */
  public boolean equals(Object o) {
    if(o instanceof EdgeUnif) { return ((t1.equals(((EdgeUnif)o).t1)) && (t2.equals(((EdgeUnif)o).t2))); }
    else { return false; }
  }
  public String toString() { return (t1.toString()) + " = " + (t2.toString()); }

}
