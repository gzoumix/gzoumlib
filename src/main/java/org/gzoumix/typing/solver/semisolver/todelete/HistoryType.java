/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  Copyright (C) 2012. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 2 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.semisolver.todelete;

import org.gzoumix.typing.solver.semisolver.edge.EdgeSemi;
import org.gzoumix.typing.solver.semisolver.edge.IEdge;
import org.gzoumix.typing.solver.term.ITerm;

public class HistoryType {

  enum Type { SEMI, UNIF };

  private Type type;
  private int semi;
  private ITerm term;

  public static HistoryType unif(ITerm t) { return new HistoryType(t); }
  public static HistoryType semi(Integer i, ITerm t) { return new HistoryType(i, t); }


  private HistoryType(ITerm t) { type = Type.UNIF; semi = 0; term = t; }
  private HistoryType(Integer i, ITerm t) { type = Type.SEMI; semi = i.intValue(); term = t; }

  public boolean validates(History h) {
    IEdge edge= h.getEdge();
    boolean has = edge.getLeft().equals(term) || edge.getRight().equals(term);

    if(has && (type == Type.UNIF)) { return true; }
    else if(has && (edge instanceof EdgeSemi) && (((EdgeSemi)edge).getI().intValue() == semi)) { return true; }
    return false;
  }

  public String toString() {
    if(type == Type.UNIF) { return " = " + term.toString(); }
    else { return " <_" + semi + " " + term.toString(); }
  }

}


