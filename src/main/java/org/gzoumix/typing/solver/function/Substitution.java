/**************************************************************************/
/*  Implementation of a simple semi-unification algorithm (Henglein 1993) */
/*  Copyright (C) 2012. Michael Lienhardt                                 */
/*                                                                        */
/*  This program is free software; you can redistribute it and/or modify  */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation; version 2 of the License.               */
/*                                                                        */
/*  This program is distributed in the hope that it will be useful, but   */
/*  WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     */
/*  General Public License for more details.                              */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with this program; if not, write to the Free Software           */
/*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA         */
/*  02110-1301 USA                                                        */
/*                                                                        */
/**************************************************************************/

package org.gzoumix.typing.solver.function;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.gzoumix.typing.solver.term.Factory;
import org.gzoumix.typing.solver.term.ITerm;
import org.gzoumix.typing.solver.term.TermVariable;

public class Substitution implements IFunction<TermVariable, ITerm> {

  private Factory _factory;
  private Map<TermVariable,ITerm> _data;
  
  public Substitution(Factory f) { _factory = f; _data = new HashMap<TermVariable,ITerm>(); }
  public Substitution(Factory f, Map<TermVariable,ITerm> data) { _factory = f; _data = data; }


  /* 1. Substitution basic Manipulation */

  public ITerm apply(ITerm t) {
    ITerm res = t;
    for(Map.Entry<TermVariable, ITerm> entry: _data.entrySet()) { res = res.subst(entry.getKey(), entry.getValue()); }
    return res;
  }

  @Override
  public Set<TermVariable> dom() {
    Set<TermVariable> res = new HashSet<TermVariable>();
    for(Map.Entry<TermVariable, ITerm> entry : _data.entrySet()) { res.add(entry.getKey()); }
    return res;
  }

  @Override
  public Set<ITerm> im() {
    Set<ITerm> res = new HashSet<ITerm>();
    for(Map.Entry<TermVariable, ITerm> entry : _data.entrySet()) { res.add(entry.getValue()); }
    return res;
  }

  @Override
  public ITerm im(TermVariable v) { return _data.get(v); }


  /* 2. Substitution Substitution */

  // puts (this o s) in this
  public void compose(Substitution s) {
    Substitution tmp = new Substitution(_factory);
    for(Map.Entry<TermVariable, ITerm> entry : s._data.entrySet()) {
      tmp._data.put(entry.getKey(), this.apply(entry.getValue()));
    }
    merge(tmp);
  }

  private void merge(Substitution s) {
    for(Map.Entry<TermVariable, ITerm> entry : s._data.entrySet()) {
      _data.put(entry.getKey(), entry.getValue());
    }
  }

  /* 3. Object */

  @Override
  public String toString() {
    String res = "\n";
    Iterator<Map.Entry<TermVariable,ITerm>> i = _data.entrySet().iterator();
    Map.Entry<TermVariable, ITerm> tmp;
    while (i.hasNext()) {
      tmp = i.next();
      res = res + "  " + tmp.getKey().toString() + " -> " + tmp.getValue().toString() + "\n";
    }
    return res;
  }
  

}
