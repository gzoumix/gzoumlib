package org.gzoumix.typing.environment;

import java.util.ArrayList;

public class EnvironmentJava {

  public enum Visibility {
    PUBLIC("public", true, true, true),
    PRIVATE("private", true, false, false),
    LOCAL("", true, true, false);

    public final String name;
    public final boolean local;
    public final boolean pack;
    public final boolean all;

    Visibility(String name, boolean local, boolean pack, boolean all) {
      this.name = name;
      this.local = local;
      this.pack = pack;
      this.all = all;
    }
  }

  public static class Path<N> extends ArrayList<N> {
    public Path() { super(); }
    public Path(ArrayList<N> path) { super(path); }
    public Path(ArrayList<N> prefix, N name) { super(prefix); this.add(name); }
  }

  public static class VisibilityPath<N> extends Path<N> {
    public final boolean star;

    public VisibilityPath(Path<N> path, boolean star) {
      super(path);
      this.star = star;
    }
  }

  // how to structure the mapping?

}
